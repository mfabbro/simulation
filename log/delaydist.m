data=load('delay30MinNoSync.csv');
close all;
hold on;
cdfplot(data(:, 4)/2);
data = data./2;

% dminout = data(data(:, 4) <= prctile(data(:,4), 97),4);
% dminout(dminout <= prctile(data(:,4), 3)) = [];
dminout = data(:,4);

lowx = -10000;
%const = 2890
const = 0

x_values = lowx:0.2:80000;
pdfnorm = fitdist(dminout - const, 'normal')
normcdf = cdf(pdfnorm,x_values);

start = 0:0.2:const;
normcdf2 = [zeros(1, length(start)) normcdf];

plot(x_values,normcdf2(1:length(x_values)),'r')