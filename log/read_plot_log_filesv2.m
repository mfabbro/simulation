%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2013 - Capstone Project MC2 - Clock Synchronisation
%
% Created by Mark Fabbro and Adair Lang, August 2013.
% Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
%
% Plot Simulation Results Function 
%
% Description: A function that produces plots from the log data files from
% a simulation.
%
% test_data = READ_PLOT_LOG_FILESV2(test_id,algorithm) takes in two strings as input that specify the
% testid and the algorithm name. It returns a struct that has all the data
% for that test inside it.
%
% The log files are assumed to be in '#{algorithm}/' with names starting
% with '#{test_id}***.txt where *** ends with either init (for initial file
% data), time (for runtime data), and test for (test parameter data).
% Assumed format for these files are
%
% #{test_id}*init.txt
    %Node name, Initial Skew, Initial Offset
    %#{name}  , #{skew}     , #{offset}
    % ...     , ...         , ...
%    
%   
% #{test_id}*test.txt
    %Nominal Hz  , #{nominal hz}
    %Synch Int   , #{synch_int}
    %Skew Range  , #{skew_range}
    %Offset Range, #{offset_range}
%   
%    
% #{test_id}*time.txt
    %#{clock_id}, #{clock_time}, #{clock_skew}, #{clock_error}, #{clock_sim_time}
    % ...       ,  ...         , ...          , ...           , ...
%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test_data=read_plot_log_filesv2(test_id,algorithm,save_figs, fig_handles)

%Default to plot all clocks for that test.
if ~exist('save_figs')
    save_figs = 0;
end
    
% if (nargin<3)
%     save_figs = 0;
% end

%List of algorthim Names
algorithm_names = {'ATSSensorNode','CarliSensorNode','FTSPSensorNode','KumarSensorNode','FTSPSensorNodev2','KumarSensorNodev2'};

%Find all corresponding Algrothims for that test
if(strcmp(algorithm,'all'))
    algorthim_id = 1:length(algorithm_names);
else
    algorthim_id = find(strcmp(algorithm_names,algorithm));
end

if(isempty(algorthim_id))
    error('read_plot_log_files:alg_name','read_plot_log_files: Invalid algorithm name');
end

%Find file info for each algorthims
for dir_no = algorthim_id
    temp_alg_name = algorithm_names{1,dir_no};
    files         = dir(temp_alg_name);
    [initial_file_data(dir_no) runtime_file_data(dir_no) test_file_data(dir_no)]= grab_file_data(files,test_id);
    
end

%Get all data into test_data structure.
for alg_no = algorthim_id
  %Import Data
    testcond1                        = importdata([algorithm_names{1,alg_no} ,'/',test_file_data(alg_no).name]);
    initset1                         = importdata([algorithm_names{1,alg_no} ,'/',initial_file_data(alg_no).name]);
    runtime_data                     = importdata([algorithm_names{1,alg_no} ,'/',runtime_file_data(alg_no).name]);
  %Place it into appropiate parts of struct  
    test_date(alg_no).nominal_hz     = testcond1.data(1);
    test_date(alg_no).sync_int       = testcond1.data(2);
    test_date(alg_no).skew_range     = testcond1.data(3);
    test_date(alg_no).offset_range   = testcond1.data(4);
    test_data(alg_no).method         = algorithm_names{1,alg_no};
    test_data(alg_no).test_id        = test_id;
    test_data(alg_no).no_nodes       = length(initset1.data);
    test_data(alg_no).node_names     = initset1.textdata(2:test_data(alg_no).no_nodes+1);
    test_data(alg_no).initial_skew   = initset1.data(:,1);
    test_data(alg_no).initial_offset = initset1.data(:,2);
    
    %loop over each node to find clock data for each node.
    for j=1:test_data(alg_no).no_nodes;
        test_data(alg_no).clock_data{j}   = runtime_data(runtime_data(:,1)==(j-1),:);
        steady_value(j)                   = max(abs(test_data(alg_no).clock_data{j}(end-100:end,4)));
    end
    max_st = max(steady_value);
    %Work out the unique simulation time points
    simulation_times  = unique(runtime_data(:,5));
    unique_sim_points = length(simulation_times);

    %Calculate the max and min of error at each point
    supremum          = zeros(unique_sim_points,1);
    infinum           = zeros(unique_sim_points,1);
    
    node_pt           = ones(test_data(alg_no).no_nodes,1);
    notsteady         = 1;
    conseq_pts        = 0;
    for time_pt = 1 : unique_sim_points
        process_snapshot = [];
        for node=1:test_data(alg_no).no_nodes;
            %check to see if next clock point is the current sim point if
            %not then it must be higher since log is assumed to be in
            %order.
            current_sim_point = test_data(alg_no).clock_data{node}(node_pt(node),5);
            %this checking should allow for cases where some nodes only
            %opperate for some of the simulation and not all of it. 
            if(current_sim_point == simulation_times(time_pt))
                process_snapshot(end+1) =  test_data(alg_no).clock_data{node}(node_pt(node),4);
                while(node_pt(node)< length(test_data(alg_no).clock_data{node}(:,5)) && (test_data(alg_no).clock_data{node}(node_pt(node),5)==simulation_times(time_pt)))
                node_pt(node)           =  node_pt(node) + 1;
                end
                
                if(node_pt(node)>length(test_data(alg_no).clock_data{node}))
                    node_pt(node) = 1;
                end
            end
        end
        if( notsteady)
            if(max(abs(process_snapshot))<2*max_st)
               conseq_pts = conseq_pts + 1;
               end_pt     = time_pt;
               if(conseq_pts>100)
                   start_pt  = end_pt-100;
                   notsteady = 0;
               end
            else
                conseq_pts = 0;
            end
        end
        supremum(time_pt) = max(process_snapshot);
        infinum(time_pt)  = min(process_snapshot);
    
    end
    
    
end
nominal_clock=test_date(alg_no).nominal_hz;
plot_data(test_data(alg_no).no_nodes,test_data(alg_no).clock_data,start_pt)
if(save_figs)
    savefigures(algorithm_names(alg_no),test_id);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Function that grabs file data matching the corresponding test and returns
%them in intial_file and log_file and test_file
    function [initial_file log_file test_file] = grab_file_data(files,test_name)
        initial_file = [];
        log_file     = [];
        test_file    = [];
        for file_no=1:length(files)
            if (length(files(file_no).name)<5)
                continue;
            elseif(strcmp(files(file_no).name(1:length(test_name)),test_name))
                if strcmp(files(file_no).name(end-7:end-4),'init')
                    initial_file=files(file_no);
                elseif strcmp(files(file_no).name(end-7:end-4),'time')
                    log_file=files(file_no);
                elseif strcmp(files(file_no).name(end-7:end-4),'test')
                    test_file=files(file_no);
                end
            end
         end
        if(isempty(initial_file) || isempty(log_file) || isempty(test_file))
            error('read_plot_log_files:test_name','read_plot_log_files: Invalid test id or not all log files')
        end
    end


%PLOT STUFF
    function plot_data(no_nodes, clock_info,start_pt)
        close all
        interval=int32(64/no_nodes);
        extra=no_nodes-64/interval;
        colors=colorcube;
        if extra<0
            extra=0;
        end
        
        if ~exist('fig_handles')
            figure(1); h1 = subplot(211); h2 = subplot(212);
            figure(2); h3 = axes();
            figure(3); h4 = axes();
            figure(4); h5 = axes();
            fig_handles = [h1, h2, h3, h4, h5];
        end
        
        %Set color order
        for n = 1:4
            set(fig_handles(n),'ColorOrder',[colors(1:extra,:);colors(extra+1:2:end,:)])
        end
        
        %Figure 1 - Two subfigures with initial error plots
        first_n_samples = start_pt;
        for node=1:no_nodes
            semilogy(fig_handles(1), clock_info{node}(1:first_n_samples,5)/(nominal_clock*60),abs(clock_info{node}(1:first_n_samples,4)));
                hold(fig_handles(1), 'all');
        end
        title(fig_handles(1), ['Virtual Clock Error - 1st ' num2str(clock_info{node}(first_n_samples,5)/(nominal_clock*60)) ' mins'])
        xlabel(fig_handles(1), 'Time (min)')
        ylabel(fig_handles(1), 'Error-ticks (log scale)')
        hold(fig_handles(1), 'off');
        
   
       % first_n_samples = 1200;
       bounds1 = mean(supremum(first_n_samples:end));
       bounds2 = mean(infinum(first_n_samples:end));
        for node=1:no_nodes
            plot(fig_handles(2), clock_info{node}(1:first_n_samples,5)/(nominal_clock*60),clock_info{node}((1:first_n_samples),4));
            hold(fig_handles(2), 'all');
        end
        title(fig_handles(2),['Virtual Clock Error - 1st ' num2str(clock_info{node}(first_n_samples,5)/(60*nominal_clock)) ' mins'])
        xlabel(fig_handles(2),'Time (min)')
        ylabel(fig_handles(2),'Error-ticks (linear scale)')
        hold(fig_handles(2), 'off');
        
        
        %Figure 2 Virtual Clock error in steady state (after a start up
        %period)
        for node=1:no_nodes
            h(node)=stem(fig_handles(3), clock_info{node}(first_n_samples:end,5)/(60*nominal_clock),clock_info{node}(first_n_samples:end,4));
            hold(fig_handles(3), 'all');
 %           legend1{node}=num2str(clock_info{node}(1));
        end
%        legend(h,legend1
        hold(fig_handles(3), 'off');
        title(fig_handles(3), 'Virtual Clock Error near Steady State')
        xlabel(fig_handles(3), 'Time (min)')
        ylabel(fig_handles(3), 'Error-ticks')
        
        hold(fig_handles(4), 'off');
        for node=1:no_nodes
            h(node)=plot(fig_handles(4), clock_info{node}(:,5)/(60*nominal_clock),clock_info{node}(:,3));
            hold(fig_handles(4), 'all');
         %   legend1{node}=clock_info{node}(1);
        end
       % legend(h,legend1)
        title(fig_handles(4), 'Delta Ratios')
        xlabel(fig_handles(4), 'Time (min)')
        ylabel(fig_handles(4), 'Delta Ratios')
        hold(fig_handles(4), 'off');

        
        %Figure 4: the plot of max and min error at each sim time to show
        %bounds.
        hold(fig_handles(5), 'all');
        plot(fig_handles(5),simulation_times(first_n_samples:end)/(60*nominal_clock), supremum(first_n_samples:end),'k');
        plot(fig_handles(5),simulation_times(first_n_samples:end)/(60*nominal_clock), infinum(first_n_samples:end),'k');
        h1=plot(fig_handles(5),clock_info{node}(first_n_samples:end,5)/(60*nominal_clock),bounds1*ones(size(clock_info{node}(first_n_samples:end,5))),'r-.','LineWidth',2);
        h2=plot(fig_handles(5),clock_info{node}(first_n_samples:end,5)/(60*nominal_clock),bounds2*ones(size(clock_info{node}(first_n_samples:end,5))),'m-.','LineWidth',2);
        legend(fig_handles(5), [h1,h2],['Mean Suprema: ' num2str(bounds1)],['Mean Infima: ' num2str(bounds2)])
        title(fig_handles(5), {'Suprema/Infima of the Virtual Clock Error ensemble near Steady State', ['Steady State time = ' num2str(num2str(clock_info{node}(first_n_samples,5)/(nominal_clock*60))) ' mins']})
        xlabel(fig_handles(5), 'Time (min)')
        ylabel(fig_handles(5), 'Error-ticks')
        hold(fig_handles(5), 'off');

    
    end

    function savefigures(alg_name,test)
        print2eps([alg_name{1} '\' test '_log_error'],1);
        print2eps([alg_name{1} '\' test '_error'],2);
        print2eps([alg_name{1} '\' test '_skews'],3);
        print2eps([alg_name{1} '\' test '_infsum'],4);
    end
end

