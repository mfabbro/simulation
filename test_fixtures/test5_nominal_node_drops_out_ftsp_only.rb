# 
#  test1_ten_sensors_no_network_delay.rb
#  simulation
#  
#  Created by Mark Fabbro and Adair Lang, May 2013
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
# 

require 'csv'
require '../framework/log_helper_methods.rb'
require '../framework/ats.rb'
require '../framework/carli.rb'
require '../framework/ftsp.rb'
require '../framework/kumar.rb'
require '../framework/delay_generator.rb'

TEST_NAME = 'test5'
mode      = 'fixed_ics'
# Set Simulation Constants.
no_of_sensors    = 10

synch_int        = 1000  # synchronisation inteveral (msec)
nominal_hz       = 10e6  # nominal clock speed (Hz)
skew_range       = 0.1  # Skew range for initial conditions
max_offset       = 1*nominal_hz #Initial Offset Range (clock ticks)
delay_generator  = DelayGenerator.new(:normal, 5,2,0)
total_simulation = (synch_int/1000.0)*nominal_hz*600 # simulate for 100 synch_int
puts "Nomial Hz is #{nominal_hz} and total_simulation is #{total_simulation}"
sensor_setups    = {
  #:ATSSensorNode   => [0.2, 0.5, 0.5,0.99], #rho_eta, rho_v, rho_o all default
  #:CarliSensorNode => [true], #control_limit=false, mean_delay=nil, control_method="Metropolis", control_gain=nil
  :FTSPSensorNode  => [8, 10], # Initial buffer = 8, root_timeout = 10, startup time= 8
  #:KumarSensorNode  => [] #Default forgetting factor and alpha tol.
}

# generate 10 sensors local clocks.
clocks = []
node_names = []
if (mode == 'fixed_ics')
  no_of_sensors.times do |x|
    skew   = 1 + (-skew_range + (x*2*skew_range/no_of_sensors))
    offset =  (x*max_offset/no_of_sensors) #max_offset/2.0 -
    name   = "node-#{x}"
    clocks << [name, skew, offset]
    node_names << name
  end

else

  no_of_sensors.times do |x|
    skew   = 1 + (skew_range - 2*rand*skew_range)
    offset = (rand*max_offset).round
    name   = "node-#{x}"
    clocks << [name, skew, offset]
     node_names << name
  end
end

sensor_setups.each_pair do |alg_name, options|
  sensors = []
  sensors_off = []
  init_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_init.csv"
  test_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_test.csv"
  CSV.open(init_cond_log, 'wb') do |csv|
    csv << ['Node name', 'Initial Skew', 'Initial Offset']
    clocks.each do |info|
      csv << info
      sensors <<  Object::const_get(alg_name).new(info[0], nominal_hz, info[1], info[2], synch_int, *options)
    end
    end  
  CSV.open(test_cond_log, 'wb') do |csv|
   csv << ['Nominal Hz',nominal_hz ]
   csv << ['Synch Int', synch_int]
   csv << ['Skew Range',skew_range]
   csv << ['Offset Range',max_offset]
  end

  # Initialize Network
  NetworkManager.initialize_network(nominal_hz, delay_generator)
  NetworkManager.topology(:strongly_connected)
  
  # Simulate.
  puts "#{TEST_NAME}: starting #{alg_name} simulation"
  runtime_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_runtime.csv"
  CSV.open(runtime_log, "wb") do |csv|    
    while ClockManager.simulation_time < total_simulation/3.0
      next_step = EventManager.next_simulation_time
#puts "Next step is #{next_step}"
      ClockManager.set_simulation_time(next_step)
      EventManager.process_events(next_step)
      generate_log_data(sensors, csv)
    end
   
   pp sensors
   pp sensors_off
   #Turn Node 0 off
   sensors_off = sensors.delete_at(0)
   new_network = [[node_names.delete_at(0)], node_names] #Creates a network where node 0 is not connected to anyone.
   pp new_network
   NetworkManager.topology(:custom, new_network)
   pp sensors
   pp sensors_off


   while ClockManager.simulation_time < 2*total_simulation/3.0
      next_step = EventManager.next_simulation_time
#puts "Next step is #{next_step}"
      ClockManager.set_simulation_time(next_step)
      EventManager.process_events(next_step)
      generate_log_data(sensors, csv)
    end
    
    #Turn off sensors back on
    sensors << sensors_off
    sensors.sort!{|a,b| a.read_my_id <=> b.read_my_id }
    pp sensors
    #Reconnect network
    NetworkManager.topology(:strongly_connected)
   while ClockManager.simulation_time < total_simulation
     next_step = EventManager.next_simulation_time
#puts "Next step is #{next_step}"
     ClockManager.set_simulation_time(next_step)
     EventManager.process_events(next_step)
     generate_log_data(sensors, csv)
   end
    
    
  end

   
  puts "#{TEST_NAME}: completed #{alg_name} simulation"
  
  # Clean up Mangers before next test.
  NetworkManager.clean_up
  EventManager.clean_up
  ClockManager.clean_up
end