# 
#  gui_driven_simulation.rb
#  simulation
#  
#  Created by Mark Fabbro and Adair Lang, July 2013
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
# 

require 'optparse'
require 'csv'
require '../framework/log_helper_methods.rb'
require '../framework/ats.rb'
require '../framework/carli.rb'
require '../framework/ftsp.rb'
require '../framework/ftsp_v2.rb'
require '../framework/kumar.rb'
require '../framework/delay_generator.rb'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: gui_driven_simulation.rb [options]"
  opts.on('-d', '--delay DIST',    'Delay Dist')         { |v| options[:delay_dist] = v }
  opts.on('-n', '--varone   DELAY VAR 1', 'Delay Var 1') { |v| options[:delay_v1]   = v }
  opts.on('-m', '--vartwo   DELAY VAR 1', 'Delay Var 2') { |v| options[:delay_v2]   = v}
  opts.on('-c', '--delay DIST',    'Delay Dist')         { |v| options[:delay_comp] = v }
end.parse!

if options[:delay_dist].empty? or options[:delay_v1].empty? or options[:delay_v2].empty? or options[:delay_comp].empty?
  raise "Need to pass in all delay variables before test can commence"
else
  options[:delay_v1] = options[:delay_v1].to_f
  options[:delay_v2] = options[:delay_v2].to_f
end

pp options
TEST_NAME = 'gui_test'

# Set Simulation Constants.
no_of_sensors    = 16
synch_int        = 1000*30  # synchronisation inteveral (msec)
nominal_hz       = 20e3            # nominal clock speed (Hz)
skew_range       = 0.1            # Skew range for initial conditions
max_offset       = 100*nominal_hz #Initial Offset Range (clock ticks)

# Toggle Delay Through Ruby input Arguments.
delay_generator  = DelayGenerator.new(options[:delay_dist].to_sym, options[:delay_v1], options[:delay_v2], 0)

#4.3536264, 6.7319259 ,2.89) #Experimental data approx - normal mean = 4.3536, var = 1.6108**2

total_simulation = (synch_int/1000.0)*nominal_hz*600
puts "Nomial Hz is #{nominal_hz} and total_simulation is #{total_simulation}"
if options[:delay_comp] == 'on'
  sensor_setups    = {
    :ATSSensorNode     => [0.7, 0.7, 0.7,0.9], #rho_eta, rho_v, rho_o all default
    :CarliSensorNode   => [true , true, "Metropolis", nil, 0.9], #control_limit=false, mean_delay=nil, control_method="Metropolis", control_gain=nil
    :FTSPSensorNodev2  => [8, 10, 0.9, true], # Initial buffer = 8, root_timeout = 10, startup time= 8
    :KumarSensorNode   => [] #Default root_timeout = 10, delay_forget_fact = 0.995, skew_forget_fact = 0.99
  }
else
  sensor_setups    = {
    :ATSSensorNode     => [0.7, 0.7, 0.7,nil], #rho_eta, rho_v, rho_o all default
    :CarliSensorNode   => [true , false, "Metropolis", nil, 0.9], #control_limit=false, mean_delay=nil, control_method="Metropolis", control_gain=nil
    :FTSPSensorNodev2  => [8, 10, 0.9,false], # Initial buffer = 8, root_timeout = 10, startup time= 8
    :KumarSensorNode   => [] #Default root_timeout = 10, delay_forget_fact = 0.995, skew_forget_fact = 0.99
  }
end
# generate sensor's local clocks.
clocks = []
no_of_sensors.times do |x|
  skew   = 1 + (-skew_range + (x*2*skew_range/no_of_sensors))
  offset =  (-x*max_offset/no_of_sensors) 
  name   = "node-#{x}"
  clocks << [name, skew, offset]
end

sensor_setups.each_pair do |alg_name, options|
  sensors = []
  init_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_init.csv"
  test_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_test.csv"
  CSV.open(init_cond_log, 'wb') do |csv|
    csv << ['Node name', 'Initial Skew', 'Initial Offset']
    clocks.each do |info|
      csv << info
      sensors <<  Object::const_get(alg_name).new(info[0], nominal_hz, info[1], info[2], synch_int, *options)
    end
  end  
  
  CSV.open(test_cond_log, 'wb') do |csv|
   csv << ['Nominal Hz',nominal_hz ]
   csv << ['Synch Int', synch_int]
   csv << ['Skew Range',skew_range]
   csv << ['Offset Range',max_offset]
  end
  
  # Initialize Network
  NetworkManager.initialize_network(nominal_hz, delay_generator)

  # Simulate.
  puts "#{TEST_NAME}: starting #{alg_name} simulation"
  runtime_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_runtime.csv"
  CSV.open(runtime_log, "wb") do |csv|    
    logcount = 0
    [total_simulation/2, total_simulation].each_with_index do |milestone, i|
      NetworkManager.topology(:custom_file, "../test_fixtures/gui_tops/top_#{i+1}.csv")
      while ClockManager.simulation_time < milestone
        next_step = EventManager.next_simulation_time
        ClockManager.set_simulation_time(next_step)
        EventManager.process_events(next_step)
        generate_log_data(sensors, csv) if (((logcount += 1) % 16) == 0)
      end
    end
  end
  puts "#{TEST_NAME}: completed #{alg_name} simulation"
  
  # Clean up Mangers before next test.
  NetworkManager.clean_up
  EventManager.clean_up
  ClockManager.clean_up
end