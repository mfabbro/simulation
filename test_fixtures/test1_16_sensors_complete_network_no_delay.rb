# 
#  test1_ten_sensors_no_network_delay.rb
#  simulation
#  
#  Created by Mark Fabbro and Adair Lang, May 2013
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
# 

require 'csv'
require '../framework/log_helper_methods.rb'
require '../framework/ats.rb'
require '../framework/carli.rb'
require '../framework/ftsp.rb'
require '../framework/ftsp_v2.rb'
require '../framework/kumar.rb'
require '../framework/delay_generator.rb'

TEST_NAME = 'test1_complete'
mode      = 'fixed_ics'
# Set Simulation Constants.
no_of_sensors    = 16

synch_int        = 3*60*1000  # synchronisation inteveral (msec)
nominal_hz       = 2e4 # nominal clock speed (Hz)
skew_range       = 0.01 # Skew range for initial conditions
max_offset       = 30*60*nominal_hz #Initial Offset Range (clock ticks)
delay_generator  = DelayGenerator.new(:static,0)#:normal,4.3536264, 6.7319259 ,2.89) #Experimental data approx - normal mean = 4.3536, var = 1.6108**2
total_simulation = (synch_int/1000.0)*nominal_hz*500 # simulate for 100 synch_int
puts "Nomial Hz is #{nominal_hz} and total_simulation is #{total_simulation}"
sensor_setups    = {
  :ATSSensorNode     => [0.7, 0.7, 0.7,0.5], #rho_eta, rho_v, rho_o all default
  :CarliSensorNode   => [true , true, "Metropolis", nil,0.5  ], #control_limit=false, mean_delay=nil, control_method="Metropolis", control_gain=nil
  :FTSPSensorNode    => [8, 10, 0.5,true], # Initial buffer = 8, root_timeout = 10, startup time= 8
  :FTSPSensorNodev2  => [8, 10, 0.5,true], # Initial buffer = 8, root_timeout = 10, startup time= 8
  :KumarSensorNode   => [] #Default root_timeout = 10, delay_forget_fact = 0.995, skew_forget_fact = 0.99
}

# generate 10 sensors local clocks.
clocks = []
if (mode == 'fixed_ics')
  no_of_sensors.times do |x|
    skew   = 1 + (-skew_range + (x*2*skew_range/no_of_sensors))
    offset =  (-x*max_offset/no_of_sensors) #max_offset/2.0 -
    name   = "node-#{x}"
    clocks << [name, skew, offset]
  end

else

  no_of_sensors.times do |x|
    skew   = 1 + (skew_range - 2*rand*skew_range)
    offset = (rand*max_offset).round
    name   = "node-#{x}"
    clocks << [name, skew, offset]
  end
end

sensor_setups.each_pair do |alg_name, options|
  sensors = []
  init_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_init.csv"
  test_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_test.csv"
  CSV.open(init_cond_log, 'wb') do |csv|
    csv << ['Node name', 'Initial Skew', 'Initial Offset']
    clocks.each do |info|
      csv << info
      sensors <<  Object::const_get(alg_name).new(info[0], nominal_hz, info[1], info[2], synch_int, *options)
    end
    end  
  CSV.open(test_cond_log, 'wb') do |csv|
   csv << ['Nominal Hz',nominal_hz ]
   csv << ['Synch Int', synch_int]
   csv << ['Skew Range',skew_range]
   csv << ['Offset Range',max_offset]
  end

  # Initialize Network
  NetworkManager.initialize_network(nominal_hz, delay_generator)
  NetworkManager.topology(:strongly_connected)
  
  # Simulate.
  puts "#{TEST_NAME}: starting #{alg_name} simulation"
  runtime_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_runtime.csv"
  CSV.open(runtime_log, "wb") do |csv|    
    count = 0
    while ClockManager.simulation_time < total_simulation
      next_step = EventManager.next_simulation_time
#puts "Next step is #{next_step}"
      ClockManager.set_simulation_time(next_step)
      EventManager.process_events(next_step)
      generate_log_data(sensors, csv) if ((count += 1)%8)==0
    end
  end
  puts "#{TEST_NAME}: completed #{alg_name} simulation"
  
  # Clean up Mangers before next test.
  NetworkManager.clean_up
  EventManager.clean_up
  ClockManager.clean_up
end