require 'csv'
require '../framework/delay_generator.rb'
# Include each sensor algorithm we'd like to test.
require '../framework/generic_sensor_template.rb'

# Set Simulation Constants.
synch_int        = 1000  # synchronisation inteveral (msec)
nominal_hz       = 10e6  # nominal clock speed (Hz)
total_simulation = nominal_hz*300 # simulate for 300 seconds.

# Create multiple sensor instances, setting their names, nominal clocks frequency,
# skews, offsets and synchronisation intervals.

sensors = []
sensors << GenericSensorTempate('node-1', nominal_hz, 1.1, 1000, synch_int)
sensors << GenericSensorTempate('node-2', nominal_hz, 1.2, 2000, synch_int)
sensors << GenericSensorTempate('node-3', nominal_hz, 1.3, 3000, synch_int)

# Initialize Network, with a delay, specified in milliseconds of the simulation 
# clock; the following are examples of specifiying a delay distribution.

delay_generator = DelayGenerator.new(:static, 0)
delay_generator = DelayGenerator.new(:uniform, 1, 2) # (min, max)
delay_generator = DelayGenerator.new(:normal, 1, 3)  # (mean, var)

NetworkManager.initialize_network(nominal_hz, delay_generator)

# Examples of setting the topology of a network.
NetworkManager.topology(:strongly_connected)
NetworkManager.topology(:custom, [['node-1', 'node-2']], ['node-3']])
NetworkManager.topology(:custom_file, "../test_fixtures/test2_topology.csv")
  
  
# Now actually perform the test and store the output in a CSV file.
# Simulate.
runtime_log = "../log/example_runtime.csv"
CSV.open(runtime_log, "wb") do |csv|    

  # Loop until the simulation time is beyond the total_simulation time.
  while ClockManager.simulation_time < total_simulation
    # Ask the event_manager the simulation time corresponding to the next event.
    next_step = EventManager.next_simulation_time
    # Advance all the clocks.
    ClockManager.set_simulation_time(next_step)
    # Fire all events that correspond to this simulation time.
    EventManager.process_events(next_step)
    # All events corresponding to this simulation time have fired. Ask each 
    # sensor to provide logging information to be stored.
    sensors.each do |s|
      csv << s.log
    end
    
    # If desired, the Network.topology could be changed in here as well.
  end
end
# Test has completed.