require 'csv'
require '../framework/log_helper_methods.rb'
require '../framework/ats.rb'
require '../framework/carli.rb'
require '../framework/ftsp.rb'
require '../framework/kumar.rb'
require '../framework/delay_generator.rb'

TEST_NAME = 'test4'

# Set Simulation Constants.
no_of_sensors    = 10
synch_int        = 1000  # synchronisation inteveral (msec)
nominal_hz       = 10e6  # nominal clock speed (Hz)
delay_generator  = DelayGenerator.new(:normal, 5,2)
total_simulation = (synch_int/1000)*nominal_hz*200 # simulate for 100 synch_int
sensor_setups    = { 
  #:ATSSensorNode   => [0.7, 0.7, 0.7], #rho_eta, rho_v, rho_o all default
  #:CarliSensorNode => [], #control_gain = default
  :FTSPSensorNode  => [8, 10],# Initial buffer = 8, root_timeout = 10, startup time= 8} #rho_eta, rho_v, rho_o all default
  :KumarSensorNode => []}
# generate 10 sensors local clocks.
clocks = []
node_names = []
skew_range = 0.05
max_offset = nominal_hz
no_of_sensors.times do |x|
  skew   = 1 + (skew_range - 2*rand*skew_range)
  offset = (rand*max_offset).round
  name   = "node-#{x}"
  clocks << [name, skew, offset]
  node_names << name
end

sensor_setups.each_pair do |alg_name, options|
  sensors = []
  init_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_init.csv"
  test_cond_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_test.csv"
  CSV.open(init_cond_log, 'wb') do |csv|
    csv << ['Node name', 'Initial Skew', 'Initial Offset']
    clocks.each do |info|
      csv << info
      sensors <<  Object::const_get(alg_name).new(info[0], nominal_hz, info[1], info[2], synch_int, *options)
    end

  end 
  CSV.open(test_cond_log, 'wb') do |csv|
   csv << ['Nominal Hz',nominal_hz ]
   csv << ['Synch Int', synch_int]
   csv << ['Skew Range',skew_range]
   csv << ['Offset Range',max_offset]
  end  

  # Initialize Network
  NetworkManager.initialize_network(nominal_hz, delay_generator)
  # Create a network of two components.
  network = node_names.each_slice((node_names.length/2.0).ceil).to_a
  NetworkManager.topology(:custom, network)
  
  # Simulate.
  puts "#{TEST_NAME}: starting #{alg_name} simulation"
  runtime_log = "../log/#{alg_name.to_s}/#{TEST_NAME}_runtime.csv"
  CSV.open(runtime_log, "wb") do |csv|    
    while ClockManager.simulation_time < total_simulation/2
      next_step = EventManager.next_simulation_time
      ClockManager.set_simulation_time(next_step)
      EventManager.process_events(next_step)
      generate_log_data(sensors, csv)
    end
    
    NetworkManager.topology(:strongly_connected)
    while ClockManager.simulation_time < total_simulation
      next_step = EventManager.next_simulation_time
      ClockManager.set_simulation_time(next_step)
      EventManager.process_events(next_step)
      generate_log_data(sensors, csv)
    end

  end
  puts "#{TEST_NAME}: completed #{alg_name} simulation"
  
  # Clean up Mangers before next test.
  NetworkManager.clean_up
  EventManager.clean_up
  ClockManager.clean_up
end