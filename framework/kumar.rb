#
#  kumar.rb
#  simulation
#
#  Created by Mark Fabbro and Adair Lang, June 2013.
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
#

require '../framework/network_manager.rb'
require '../framework/event_management.rb'


class KumarSensorNode
  #KumarSensor Node
  def initialize(name, nom_crystal_hz, skew, offset, synch_int, root_timeout = 10,delay_forget_fact=0.6, skew_forget_fact=0.99)
    @name              = name
    @my_id             = name.match(/\w+-(\d+)/)[1].to_i
    @clock             = ClockManager.new(nom_crystal_hz, skew, offset, 0)
    @net_manager       = NetworkManager.new(self, name)
    @event_manager     = EventManager.new(self, @clock)
    @synch_int         = synch_int
    @nom_crystal_hz    = nom_crystal_hz
    @delay_forget_fact = delay_forget_fact
    @skew_forget_fact  = skew_forget_fact
    @root_timeout      = root_timeout
    
    @current_root       = @my_id
    @bc_no              = 1
    @bc_since_last_root = 0
    @old_root_bc_no     = 0
    @old_root           = @my_id
    @beta_hat           = 0        #Correction factor of offset relative to the nominal clock. 
    @alpha_hat          = 1

    #A structure for storeing data associated with a neighbour
    #n_* means the data is associated with the neighbour and not self
    @kumar_data = Struct.new(:delay,
                             :delay_gain,
                             :delta_me_recv,
                             :initialize_rls,
                             :last_me_recv,
                             :last_me_sent,
                             :n_alpha_hat,
                             :n_beta_hat,
                             :n_delta_sent,
                             :n_last_sent,
                             :n_pw_offset,
                             :pw_alpha,
                             :pw_init_recvd,
                             :pw_offset,
                             :pw_resp_recvd,
                             :skew_rls_gain)
    #nd is the Hash that stores a struct for every neighbour node talks to.
    @nd = Hash.new 
    @event_manager.add_timed_event(@synch_int, :broadcast)

  end
  
  def check_nd(neigh)
    #A method to ensure a new object is created if data for neigh doesn't exist yet.
    @nd[neigh] = (@nd[neigh]==nil) ? @kumar_data.new : @nd[neigh]
  end

  def broadcast
    #Check to see if root has timed out
    if(@bc_since_last_root > @root_timeout)
     @old_root           = @current_root
     @old_root_bc_no     = @bc_no
     @current_root       = @my_id
     @bc_since_last_root = 0
    end
    
    @net_manager.neighbours.each do |n|
      check_nd(n)
      pw_init(n)
    end
    
    #Increase bc_no if root otherwise increase counter since last root
    if(@current_root == @my_id)
     @bc_no += 1
    else
     @bc_since_last_root += 1
    end
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end

  def pw_init(node_name)
    # Create a Kumar Network Packet for neighbour node_name
    packet = {:name          => @name,
              :id            => @my_id,
              :beta_hat      => @beta_hat,
              :root_id       => @current_root,
              :root_bc_no    => @bc_no,
              #Sending zero as the pw_offset if we don't have one.
              :pw_offset     => (@nd[node_name][:pw_offset] || 0),
              :alpha_hat     => @alpha_hat,
              :mesg_type     => "pw_init"} #To specify type of message
    @net_manager.send_packet(node_name, Marshal.dump(packet))
    @nd[node_name][:last_me_sent] = @clock.time 
  end

  def pw_response(time_received, mesg)
    #Create a response packet to pairwise_init request. 
    packet = {:name               => @name,
              :id                 => @my_id,
              :time_sent          => @clock.time,
              :virt_time_received => time_received*@alpha_hat,
              :mesg_type          => "pw_resp"}
    @net_manager.send_packet(mesg[:name], Marshal.dump(packet))
  end
  
  def recv_packet(mesg)
    
    time_received = @clock.time
    mesg          = Marshal.load(mesg)
    neigh         = mesg[:name]
    #Check to see if nd[neigh] exists
    check_nd(neigh)
    
    #Receive a pw_init - then send response and record relavent data
    if (mesg[:mesg_type] == "pw_init")
        pw_response(time_received,mesg)
        @nd[neigh][:n_beta_hat]  = mesg[:beta_hat]
        @nd[neigh][:n_alpha_hat] = mesg[:alpha_hat]
        @nd[neigh][:n_pw_offset] = mesg[:pw_offset]

        if(mesg[:root_id] < @current_root)
          if ((mesg[:root_id] == @old_root) && (mesg[:root_bc_no]== @old_root_bc_no)) 
            return #if it is the old root with old bc_no then ignore.
          end 
          @current_root = mesg[:root_id]
          @bc_no        = mesg[:root_bc_no]

        elsif(mesg[:root_id] == @current_root && @bc_no < mesg[:root_bc_no])
          @bc_since_last_root = 0
          @bc_no              = mesg[:root_bc_no]
        end
          
        @nd[neigh][:pw_init_recvd] = true
    
    #Receive a pw_reponse - then do pw_update
    elsif (mesg[:mesg_type] == "pw_resp")
        update_pw(time_received, mesg)
    end
    
    unless (@current_root==@my_id) #Don't do update algorithm if you are the root
      do_spatial_smoothing
    end
  
  end

  def update_pw(time_received, mesg)
    neigh = mesg[:name] #Work out which neighbour to update
    
    if(@nd[neigh][:n_last_sent] == nil) #Check to see if have received packet from this neighbour before
      
      @nd[neigh][:initialize_rls]   = true
        
    else
      #Update time intervals
      @nd[neigh][:n_delta_sent]   = mesg[:time_sent] - @nd[neigh][:n_last_sent]
      @nd[neigh][:delta_me_recv]  = time_received    - @nd[neigh][:last_me_recv]
      
      if(@nd[neigh][:initialize_rls]) #If it is first update then need to initialise
        @nd[neigh][:initialize_rls] = false
        @nd[neigh][:pw_alpha]       = (1.0*@nd[neigh][:n_delta_sent])/@nd[neigh][:delta_me_recv]
        @nd[neigh][:skew_rls_gain]  = 1.0/@nd[neigh][:delta_me_recv]**2
        @nd[neigh][:delay]          = 0
        @nd[neigh][:delay_gain]     = 1.0
      else
        #Update pairwise alpha estimate using RLS
        @nd[neigh][:skew_rls_gain] = (@nd[neigh][:skew_rls_gain]*@skew_forget_fact)/(1.0+@skew_forget_fact*@nd[neigh][:skew_rls_gain]*(@nd[neigh][:delta_me_recv]**2))
        @nd[neigh][:pw_alpha]      = @nd[neigh][:pw_alpha] + @nd[neigh][:skew_rls_gain]*@nd[neigh][:delta_me_recv]*(@nd[neigh][:n_delta_sent]-@nd[neigh][:delta_me_recv]*@nd[neigh][:pw_alpha])
     
      end

      #Update delay estimate using RLS assuming delay is symmeteric. 
      @nd[neigh][:delay]         = @nd[neigh][:delay] + @nd[neigh][:delay_gain]*((1.0/2.0)*((time_received - @nd[neigh][:last_me_sent])) - @nd[neigh][:delay])
      @nd[neigh][:delay_gain]    = @nd[neigh][:delay_gain]/(@delay_forget_fact + @nd[neigh][:delay_gain])
      
      #Update pw_offset using time*alpha_hat data
      @nd[neigh][:pw_offset]     =  mesg[:virt_time_received] - ((time_received-@nd[neigh][:delay])*@alpha_hat).to_i 
      #@nd[neigh][:pw_offset]    =  (mesg[:virt_time_received] - (@nd[neigh][:last_me_sent] + @nd[neigh][:delay])*@alpha_hat).to_i
            
      #Acknowledge receiving pw for this neigh
      @nd[neigh][:pw_resp_recvd] = true
    end
      
      #Update the last neigh send time and last received time

      @nd[neigh][:n_last_sent]   = mesg[:time_sent]
      @nd[neigh][:last_me_recv]  = time_received
    
  end
  
  def do_spatial_smoothing
    ready = true
    
    #Check to see if have received all pw_init and repons from every neighbour
    @net_manager.neighbours.each do |n|
      unless (not(@nd[n].nil?) and @nd[n][:pw_init_recvd] and @nd[n][:pw_resp_recvd])
          ready = false
        break
      end
    end
    
    if(ready)
      #Sum pairwise offsets and offsets of neighs and reset acknowledgements 
      alpha_temp  = 0.0
      offset_temp = 0
      @net_manager.neighbours.each do |n|
        alpha_temp            += Math.log(@nd[n][:n_alpha_hat]) + Math.log(@nd[n][:pw_alpha])
        offset_temp           += -@nd[n][:n_beta_hat] + @nd[n][:n_pw_offset]
        @nd[n][:pw_init_recvd] = false
        @nd[n][:pw_resp_recvd] = false
      end
      
      degree  = @net_manager.neighbours.length
      
      #If there are no neighbours then declare self as the root and stop updating.
      if(degree == 0)
        @old_root           = @current_root
        @old_root_bc_no     = @current_bc_no
        @current_root       = @my_id
        @bc_since_last_root = 0
        return
      end
      #Update clock parameters. 
      @alpha_hat          = Math.exp((alpha_temp/degree))
      @beta_hat           = (-offset_temp)/degree

    end

  end

  def virtual_clock
    @alpha_hat*@clock.time + @beta_hat
  end

  def log
    [@my_id, virtual_clock, @alpha_hat , ClockManager.simulation_time]
  end
end

if __FILE__ == $0
  # All kumar.rb testing now preformed using test_fixtures
end
