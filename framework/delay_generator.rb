require 'pp'
class DelayGenerator
  def initialize(delay_type, *dist_stats)  
    case delay_type
    when :static
      @delay_func = lambda { return dist_stats[0] }
    when :uniform
      min_delay, max_delay = dist_stats
      puts min_delay, max_delay
      @delay_func = lambda { 
        return (min_delay + Kernel.rand*(max_delay - min_delay))}
    when :normal
      mean, var, min = dist_stats
      raise "Non-Negative means and variances pwease" if mean < 0 or var < 0
      @normal_obj = RandomGaussian.new(mean, Math.sqrt(var))
      @delay_func = lambda { 
        val = @normal_obj.rand
        val = val<min ? min : val #cap min value.
        return val
      }
    else
      raise "Unsupported delay_type:#{delay_type}"
    end
  end

  def rand
    @delay_func.call
  end
end


# http://stackoverflow.com/users/513598/antonakos
class RandomGaussian
  def initialize(mean, stddev, rand_helper = lambda { Kernel.rand })
    @rand_helper = rand_helper
    @mean = mean
    @stddev = stddev
    @valid = false
    @next = 0
  end

  def rand
    if @valid then
      @valid = false
      return @next
    else
      @valid = true
      x, y = self.class.gaussian(@mean, @stddev, @rand_helper)
      @next = y
      return x
    end
  end

  private
  def self.gaussian(mean, stddev, rand)
    theta = 2 * Math::PI * rand.call
    rho = Math.sqrt(-2 * Math.log(1 - rand.call))
    scale = stddev * rho
    x = mean + scale * Math.cos(theta)
    y = mean + scale * Math.sin(theta)
    return x, y
  end
end