#
#  carli.rb
#  simulation
#
#  Created by Mark Fabbro and Adair Lang, April 2013.
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
#

require '../framework/network_manager.rb'
require '../framework/event_management.rb'

class CarliSensorNode
  #
  # NOTE: A Control Gain too large creates negative skews which completely
  # ruins the algorithms convergence (whether or not the problem is this
  # particular implementation or the carli method itself needs to be
  # investigated further).
  #
  # My hunch at the moment is that when you increase the travel time of packets
  # the synchronisation interval is being implicitly increased. This suggests
  # that the ideal control_gain should be changed to suite this new
  # psuedo-sync inverval.
  #
  # ALSO, in reality, clock skews are more than likely within (made up) 15%
  # of each other, where as our tests so far have been checking for convergence
  # on clocks with twice, and thrice speeds etc. This also forces us to minimise
  # the @control_gain as the error signals are massive.

  def initialize(name, nom_crystal_hz, skew, offset, synch_int,  control_limit=false, delay_on=true, control_method="Metropolis", control_gain=nil,forget_fact = 0.5)
    @name             = name
    @my_id            = name.match(/\w+-(\d+)/)[1].to_i
    @clock            = ClockManager.new(nom_crystal_hz, skew, offset, 0)
    @net_manager      = NetworkManager.new(self, name)
    @virtual_clock    = ClockManager.new(nom_crystal_hz, 1, offset, 0, :virtual, @clock)
    @event_manager    = EventManager.new(self, @virtual_clock)
    @synch_int        = synch_int
    @broadcast_no     = 0
    @nom_crystal_hz   = nom_crystal_hz
    @control_gain     = control_gain || 1000.0/((@synch_int*nom_crystal_hz))#Convert sync int into seconds - control_gain = 1/(2T) where T is in clock tics.
    @control_method   = control_method #When node is initialised user can specify k_ij values method. At this stage only Metropolis method is used.
    @delay            = Hash.new       #If mean delay is specified and not nil then delay compensation is used. It is assumed mean_delay is given in units of simulation clock tics.
    @delay_estimator_enabled = delay_on 
    @delay_gain       = Hash.new
    @forget_fact      = forget_fact
    @control_limiter  = control_limit
    @alpha_hat        = 1; #For testing direct method for transforming from local_clock to virt_clock
    @beta_hat         = 0;
    @event_manager.add_timed_event(@synch_int, :broadcast)
    @initialise       = control_limit ? 20 : 0
    @old_no_neighs    = 0
    @clca_neigh_buffer = {}
  end

  def broadcast
    # Create a Carli Network Packet and send to all neighbours.
    packet = {:name        => @name,
              :bc_no       => @broadcast_no,
              :time_stamp  => virtual_clock_time,
              :no_of_neigh => @net_manager.neighbours.length}

    @net_manager.neighbours.each do |n|
      @net_manager.send_packet(n, Marshal.dump(packet))
    end
    @broadcast_no += 1
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end
  
  def send_delay_packet(destination)
       packet = {:name        => @name,
                 :type        => 'delay_int',
                 :time_stamp  => @clock.time}
	@net_manager.send_packet(destination,Marshal.dump(packet))
  end
  
  def reply_delay_packet(mesg)
    packet = {:name              => @name,
	            :type              => 'delay_reply',
			        :origin_time_stamp => mesg[:time_stamp]}
   @net_manager.send_packet(mesg[:name],Marshal.dump(packet))
  end
  
  def recv_packet(mesg)
    # mesg = [name_of_node, boardcast_no, their_virtual_time, their_root_id]
    mesg = Marshal.load(mesg)
    time_received = @clock.time
    neigh         = mesg[:name]

    #Determine type of packet that has been received 
	  if(mesg[:type] == 'delay_int')
	   reply_delay_packet(mesg)
	  return
	  elsif(mesg[:type] == 'delay_reply')
	   update_delay(mesg,time_received)
	  return
	  end

    if @delay[mesg[:name]].eql?(nil)
      @delay[mesg[:name]]      = 0.0
      @delay_gain[mesg[:name]] = 1.0
    end
    
    @clca_neigh_buffer[neigh]             = {}
    @clca_neigh_buffer[neigh][:time_diff] = (mesg[:time_stamp] - virtual_clock_time+ @alpha_hat*@delay[mesg[:name]]).floor
    @clca_neigh_buffer[neigh][:k_ij]      = (1.0/([@net_manager.neighbours.length, mesg[:no_of_neigh]].max ))
    @clca_neigh_buffer[neigh][:updated]   = true
    if(@delay_estimator_enabled)
      send_delay_packet(mesg[:name])
    end



    @net_manager.neighbours.each do |n|
        return if @clca_neigh_buffer[n].nil? or not @clca_neigh_buffer[n][:updated]
    end

      do_carli
  
  end
  
  def update_delay(mesg,time_received)
      neigh               = mesg[:name]
 	    @delay[neigh]       = (@delay[neigh] + @delay_gain[neigh]*((1.0/2.0)*((time_received - mesg[:origin_time_stamp])) - @delay[neigh]))
      @delay_gain[neigh]  = @delay_gain[neigh]/(@forget_fact+@delay_gain[neigh])

  end
  
  def do_carli
    # Assuming no additional packets will be sent until all work is done at
    # once.

    # Grab all packets of the correct broadcast number from our CURRENT
    # neighbours list
    # Calculate the control signal u_i
    u_i = 0

    @net_manager.neighbours.each do |n|
      u_i += (@clca_neigh_buffer[n][:k_ij] * @clca_neigh_buffer[n][:time_diff])
      @clca_neigh_buffer[n][:updated] = false
    end

    u_i /= 2
    @beta_hat  += u_i.floor  
    
    if(@initialise>0)
      @initialise-=1 
    else
      @alpha_hat += @control_gain*u_i
      @beta_hat  -= @control_gain*u_i*@clock.time
    end

  end
  
  def virtual_clock_time
   (@alpha_hat*@clock.time + @beta_hat).floor
  end
  
  def log
    [@my_id, virtual_clock_time, @alpha_hat, ClockManager.simulation_time]
  end
end

if __FILE__ == $0
  # All carli.rb testing now preformed using test_fixtures
end
