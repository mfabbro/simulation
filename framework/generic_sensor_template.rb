require 'csv'
require '../framework/network_manager.rb'
require '../framework/event_management.rb'

class GenericSensorTemplate
  def initialize(name, nom_crystal_hz, skew, offset, synch_int, *options)
    # Store our unique ID provided by the test.
    @name             = name
    # Create a model for a clock with the skews and offsets provided.
    @clock            = ClockManager.new(nom_crystal_hz, skew, offset, 0)
    # Connect ourselves to the network using our unique ID.
    @net_manager      = NetworkManager.new(self, name)
    # Create our own event manager that operates on our own clock's timings.
    @event_manager    = EventManager.new(self, @clock)
    # Store the number of milliseconds between the algorithms synchs.
    @synch_int        = synch_int

    # >
    # Create any algorithm specific variables here.
    # or store the additonal parameters needed for the algorithm.
    # <
    
    # Example use of queueing an event using the event manager.
    # This will cause the broadcast method to be run @synch_int milliseconds 
    # (according to our own clock) in the future.
    @event_manager.add_timed_event(@synch_int, :synch_int_expired)
  end
  
  def synch_int_expired
    # Create a packet to be sent over the network.
    packet = {:name       => @name, 
              :time       => @clock.time}
    # Converts the packet object into a string.
    serialised_packet = Marshal.dump(packet)
    # Send the packet to every one of our neighbours.
    @net_manager.neighbours.each do |n|
      @net_manager.send_packet(n, serialised_packet)
    end
    # Queue up the broadcast event again.
    @event_manager.add_timed_event(@synch_int, :synch_int_expired)
  end

  # This method is required by the NetworkManager. It is called when someone
  # sends a packet to this sensor.
  def recv_packet(mesg)
    # Convert the serialised mesg back into the original object that was sent.
    mesg = Marshal.load(mesg)

    # Grab the information out of the packet.
    name       = mesg[:name]
    their_time = mesg[:time]

    # > 
    # Perform algorithm related work in here. Including queuing new events,
    # sending more packets, or calling other methods to help with the work.
    # <
  end

  # This method is used by the test framework to get access to sensor data 
  # that is useful for plotting or analysis after the test is finished. 
  def log
    [@name, @clock.time, ClockManager.simulation_time]
  end
end