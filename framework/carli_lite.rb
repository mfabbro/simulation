#
#  carli.rb
#  simulation
#
#  Created by Mark Fabbro and Adair Lang, April 2013.
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
#

require '../framework/network_manager.rb'
require '../framework/event_management.rb'

class CarliSensorNode
  #
  # NOTE: A Control Gain too large creates negative skews which completely
  # ruins the algorithms convergence (whether or not the problem is this
  # particular implementation or the carli method itself needs to be
  # investigated further).
  #
  # My hunch at the moment is that when you increase the travel time of packets
  # the synchronisation interval is being implicitly increased. This suggests
  # that the ideal control_gain should be changed to suite this new
  # psuedo-sync inverval.
  #
  # ALSO, in reality, clock skews are more than likely within (made up) 15%
  # of each other, where as our tests so far have been checking for convergence
  # on clocks with twice, and thrice speeds etc. This also forces us to minimise
  # the @control_gain as the error signals are massive.

  def initialize(name, nom_crystal_hz, skew, offset, synch_int)
    @name             = name
    @my_id            = name.match(/\w+-(\d+)/)[1].to_i
    @clock            = ClockManager.new(nom_crystal_hz, skew, offset, 0)
    @net_manager      = NetworkManager.new(self, name)
    @event_manager    = EventManager.new(self, @clock)
    @synch_int        = synch_int
    @broadcast_no     = 0

    @clca_neigh_buffer = {}
    @nom_crystal_hz    = nom_crystal_hz
    @control_gain      = 1000.0/((@synch_int*nom_crystal_hz))#Convert sync int into seconds - control_gain = 1/(2T) where T is in clock tics.
    @alpha_hat         = 1; #For testing direct method for transforming from local_clock to virt_clock
    @beta_hat          = 0;
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end

  def broadcast
    # Create a Carli Network Packet and send to all neighbours.
    time_stamp = @clock.time
    packet = {:name        => @name, #
              :time_stamp  => virtual_clock_time(time_stamp),
              :no_of_neigh => @net_manager.neighbours.length}
    @net_manager.neighbours.each do |n|
      @net_manager.send_packet(n, Marshal.dump(packet))
    end
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end
  
  
  def recv_packet(mesg)
    # mesg = [node_id, their_virtual_time, their_no_of_neighbours]
    mesg = Marshal.load(mesg)
    neigh = mesg[:name]
    time_received = @clock.time
    @clca_neigh_buffer[neigh]             = {}
    @clca_neigh_buffer[neigh][:time_diff] = mesg[:time_stamp] - virtual_clock_time(time_received)
    @clca_neigh_buffer[neigh][:k_ij]      = (1.0/([@net_manager.neighbours.length, mesg[:no_of_neigh]].max + 1))
    @clca_neigh_buffer[neigh][:updated]   = true

    # Only update if received new packet from every neighbour
    @net_manager.neighbours.each do |n|
      return if @clca_neigh_buffer[n].nil? or not @clca_neigh_buffer[n][:updated]
    end
    update
  end
  
  
  def update
    # Calculate the control signal u_i
    u_i = 0
    @net_manager.neighbours.each do |n|
      u_i += (@clca_neigh_buffer[n][:k_ij] * @clca_neigh_buffer[n][:time_diff])
      @clca_neigh_buffer[n][:updated] = false
    end

    # Perform update using u_i.
    @alpha_hat += @control_gain*u_i
    @beta_hat  += u_i.floor  - @control_gain*u_i*@clock.time
  end
  
  def virtual_clock_time(time_stamp)
   @alpha_hat*time_stamp + @beta_hat
  end
  
  def log
    [@my_id, virtual_clock_time(@clock.time), @alpha_hat, ClockManager.simulation_time]
  end
end

if __FILE__ == $0
  # All carli.rb testing now preformed using test_fixtures
end
