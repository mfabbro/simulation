# 
#  network_manager.rb
#  framework
#  
#  Created by Mark Fabbro and Adair Lang, April 2013
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
# 
require 'csv'
require 'pp'
#require 'simulation_constants.rb'

##
# NetworkManager is a class that deals with the interconnect between different
# nodes. Once all Nodes have created an instance of NetworkManager, the
# singleton class +topology+ must be called to define the specific
# interconnect between the nodes.
# --
class NetworkManager
  @@node_data = {}
  # @@node_data = { node_name => {:neighbours => [neighbour_name, ...],
  #                               :obj        => obj_instance}
  #               }

  ##
  # ==== Attributes
  # * +client+ - An object instance that wishes to be connected into the
  #   network. This object must have the +recv_packet+ method in order to
  #   receive communications from the NetworkManager.
  # * +node_name+ - A String used to uniquely identify this particular node in
  #   the network. Initialize will raise an error if two identically named
  #   nodes register to the network manager.
  # --
  def initialize(client, node_name)
    @node_name = node_name
    if @@node_data.keys.include? node_name
      raise "NetworkManager:#{node_name} has already been registered"
    else
      @@node_data[node_name] = {:obj => client, :neighbours => []}
    end
  end

  def self.initialize_network(nom_crystal_hz, delay_func, drop_percentage = 0)
    @@delay_func = delay_func
    @@event_manager = EventManager.new(self, ClockManager.new(nom_crystal_hz))
    @@drop_percentage = drop_percentage
  end
  
  def self.clean_up
    @@node_data = {}
  end
  
  ##
  # This method must be called on NetworkManager after all the nodes have
  # registered plus any other time when the network topology should be
  # changed. Before this method is called all nodes are disconnected from each
  # other which means an inability to send and receive packets. 
  # ==== Attributes
  # * +top_type+ - At the moment only accepts +:strongly_connected+ but in the
  #   future will take inputs such as :custom.
  # * +options+ - If a custom topology is chosen, +options+ must be populated
  #   with the topology of the network.A topology with two isolated networks
  #   of two nodes would be:
  #     options = [['nodeA', 'nodeB'], ['nodeC', 'nodeD']]
  # --  
  def self.topology(top_type, options=nil)
    case top_type
    when :strongly_connected
      @@node_data.keys.each do |node|
        @@node_data[node][:neighbours] = @@node_data.keys.reject{|n| n.eql? node}
      end
    when :custom_file
      #options = "\dir\file.csv" Where file.csv is Adjacency matrix for network. 
      #Remove any old neighbours before createing new topology
      @@node_data.keys.each do |node|
        @@node_data[node][:neighbours] = []
      end 
      network_size = @@node_data.keys.length
        row_no = 0
        #Loop over each row, then each col in each row and add neighbour 
        #if value is a 1 (possibly not the best way to do this?)
        CSV.foreach(options) do |row|
          col_no = 0
          if row.length.eql? network_size
            row.each do |neigh|
              if(neigh=="1")
                @@node_data[@@node_data.keys[row_no]][:neighbours] << @@node_data.keys[col_no]
              end
              col_no+=1
            end
          else
            raise "Input file does not have correct dimensions"
          end
          row_no+=1
        end
    when :custom
      # options = [[node1, node2], [node3, node4], [node3, node1]]
      # pp options
      options.each do |component|
        component.each do |node|
          # pp component.reject{|n| n.eql? node}
          @@node_data[node][:neighbours] = component.reject{|n| n.eql? node}
        end
      end
    else
      raise "Unsupported Network Topology:#{top_type}"  
    end
  end
  
  ## 
  # Returns an array with the names of all neighbouring nodes in the network.
  # --
  def neighbours
    @@node_data[@node_name][:neighbours]
  end

  ##
  # The method used to send packets across the simulated network. Only
  # neighbours can be transmitted to. At the moment the method will raise an
  # exception if a non-neighbour or unknown node is used as the destination.
  # ==== Attributes
  # * +dest_node_name+ - A String used to identify a neighbouring node whom
  #   you wish to send a packet to.
  # * +mesg+ - The data you wish to send. In order to accurately simulate the
  #   packet types it would be preferred if the data was serialised. 
  # --
  def send_packet(dest_name, mesg)
    wait = @@delay_func.rand
    if @@node_data.keys.include? dest_name
      @@event_manager.add_timed_event(wait, 
        :deliver_packet, @@node_data[dest_name][:obj], mesg)
    else
      raise "Node:#{@node_name}: is trying to send a message to " +  
        "#{dest_node_name} whom isn't registered on the network."
    end
  end

  def self.deliver_packet(dest_obj, mesg)
    @shitty_connection_man = @shitty_connection_man || dest_obj 
    @count ||= 1
    unless dest_obj == @shitty_connection_man and rand < @@drop_percentage
      dest_obj.send(:recv_packet, mesg)
      @count = 0 if (dest_obj == @shitty_connection_man)  
    else
      puts "sh*tty_connection_man dropped packet #{@count}"
      @count += 1
    end
  end

  ##
  # A simple debugging method that dumps the NetworkManagers datastructures to
  # STDOUT for inspection.
  # --
  def self.dump
    pp @@node_data
    pp '--'
  end
end
