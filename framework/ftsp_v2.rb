#
#  ftsp_v2.rb
#  simulation
#
#  Created by Mark Fabbro and Adair Lang, May 2013.
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
#

require '../framework/network_manager.rb'
require '../framework/event_management.rb'
require '../framework/matrix.rb'

class FTSPSensorNodev2
  #
  # This is version of FTSP that is implemented on Arduinos and is very similar 
  # to code used on Tiny OS from
  # http://tinyos.cvs.sourceforge.net/viewvc/tinyos/tinyos-2.x/tos/lib/ftsp/
  # Assumptions: Assuming that names are always going to be 'node-xxx' where
  # xxx are numbers and unique.

  # root_timeout: If I have send root_timeout number of packets before I
  # receive a packet from the root. I presume the root is dead.

  def initialize(name, nom_crystal_hz, skew, offset, synch_int, buffer_size, root_timeout, lambda=0.999,delay_estimator = true)
    @name             = name
    @my_id            = name.match(/\w+-(\d+)/)[1].to_i
    @clock            = ClockManager.new(nom_crystal_hz, skew, offset, 0)
    @net_manager      = NetworkManager.new(self, name)
    @event_manager    = EventManager.new(self, @clock)
    @synch_int        = synch_int
    @broadcast_no     = 0


    @curr_root_est    = @my_id       # The id of who the local node thinks the root node is
    @root_change      = false        # A flag to indicate that the local node thinks the root has changed
    @root_timeout     = root_timeout
    @old_root         = @my_id       # id of old root if we take over root so we can ignore redundant old root packets.
    @old_bc_no        = 0            # the last bc_no we received from old root before time-out.
    @delay            = Hash.new     # a parameter that keeps track of estimated delay mean between 2 nodes
    @delay_gain       = Hash.new     # a gain parameter to filter new delay estimation.
    @delay_estimator  = delay_estimator #a boolean variable that specifies whether or not to use delay estimation
    
    @broadcasts_since_root = 0       # number of packets we've sent since we have received a packet from the root
    @forget_fact_delay= lambda       # forgeting factor for RLS for delay_estimation
    @skew             = 0            # skew diff between self and root
    @offsetAverage    = 0            # offset diff between self and root
    @localAverage     = 0            # local average used to shift values to be centred on zero for linear regression
    @buffer           = []           # buffer for linear regression points
    @buffer_full      = false        # buffer flag
    @max_buffer_size  = buffer_size
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end
  
  def read_my_id
  @my_id
  end
  
  def broadcast
   # if no new data has arrived with respect to root (in root_timeout syncs)
   # then assume that root has dropped out

    if (@curr_root_est != @my_id) and (@broadcasts_since_root >= @root_timeout)
        @old_root      = @curr_root_est
        @old_bc_no     = @broadcast_no
        @curr_root_est = @my_id
        @root_change   = true
    end
     # Create a FTSP Network Packet and send to all neighbours.
    packet = {:name        => @name,
              :type        => 'bc',
              :bc_no       => @broadcast_no,
              :time_stamp  => virtual_clock,
              :root_id     => @curr_root_est}

    @net_manager.neighbours.each do |n|
      @net_manager.send_packet(n, Marshal.dump(packet))
    end

    #root is the only one to increment broadcast_no
    if @curr_root_est == @my_id
        @broadcast_no += 1
    else
        @broadcasts_since_root +=1
    end
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end
  
  def send_delay_packet(destination)
       packet = {:name        => @name,
                 :type        => 'delay_int',
                 :time_stamp  => @clock.time}
  @net_manager.send_packet(destination,Marshal.dump(packet))
  end
  
  def reply_delay_packet(mesg)
    packet = {:name              => @name,
              :type              => 'delay_reply',
              :origin_time_stamp => mesg[:time_stamp]}
   @net_manager.send_packet(mesg[:name],Marshal.dump(packet))
  end
  
  def recv_packet(mesg)
    
    mesg          = Marshal.load(mesg)
    time_received = @clock.time
    
    #Determine type of packet that has been received 
    if(mesg[:type]=='delay_int')
     reply_delay_packet(mesg)
     return 
    elsif(mesg[:type]=='delay_reply')
     update_delay(mesg,time_received)
     return 
    end
    
    #Else It is a 'bc' message that needs to determined if it is relevant.
    if mesg[:root_id] < @curr_root_est #is the mesg root younger than our estimate
        if ((mesg[:root_id] == @old_root) && (mesg[:bc_no]== @old_bc_no)) 
         return #if it is the old root with old bc_no then ignore.
        end
        @curr_root_est = mesg[:root_id]
        @root_change   = true 
    elsif (@curr_root_est == @my_id) then return #If we are the root then ignore message
    
    elsif (mesg[:root_id] > @curr_root_est) or (mesg[:bc_no] <= @broadcast_no) 
    #is it older root or is it expected root with old bc_no
    return 

    end

  # If we reach here then we either have a packet 
  # from a new root or we have a new packet (bc_no) from the current root.

    #Reset bc_since_root and update bc_number.  
    @broadcasts_since_root  = 0
    @broadcast_no           = mesg[:bc_no]
    #Initialise delay to 0 (note that if delay_estimator is off then delay stays at 0).
    if(@delay[mesg[:name]]==nil)
      @delay[mesg[:name]]      = 0;
      @delay_gain[mesg[:name]] = 1;
    end
    
    #Update clock with mesg and time received
    update_clock_params(mesg,time_received)
    
    #If delay estimator is on then send a delay_int packet to update delay estimation with this neighbour.
      if(@delay_estimator)
        send_delay_packet(mesg[:name])
      end
          

  end
  
  def update_delay(mesg,time_received)
      neigh              = mesg[:name]
      @delay[neigh]      = @delay[neigh] + @delay_gain[neigh]*((1.0/2.0)*((time_received - mesg[:origin_time_stamp])) - @delay[neigh])
      @delay_gain[neigh] = @delay_gain[neigh]/(@forget_fact_delay+@delay_gain[neigh])
  end
  
  def update_clock_params(mesg,time_received)   

        time_received = time_received
        #If Root has changed since last update then need to reinitialise.
        if @root_change 
       #   puts "#{@name} dumping buffer "
         @buffer            = []
         @buffer_full       = false
         @root_change       = false
        end
          @buffer << {:time_diff => mesg[:time_stamp] + @delay[mesg[:name]]*(@skew + 1.0) - time_received, :time_received => time_received}  
        if @buffer_full
          @buffer.shift
        elsif (@buffer.length == @max_buffer_size)
           @buffer_full = true
        end
        do_linear_regression
  end


  def do_linear_regression
      n   = @buffer.length #Number of valid entries
      
      #Pick first buffer as a guess for the average
      newLocalAverage  = @buffer[0][:time_received]
      newOffsetAverage = @buffer[0][:time_diff]
      
      #Compute offsets from these averages to get actual average
      localSum          = @buffer.map{|x| (x[:time_received]- newLocalAverage) /n}.inject(:+)
      offsetSum         = @buffer.map{|x| (x[:time_diff]    - newOffsetAverage)/n}.inject(:+)
      localAverageRest  = @buffer.map{|x| (x[:time_received]- newLocalAverage) %n}.inject(:+)
      offsetAverageRest = @buffer.map{|x| (x[:time_diff]    - newOffsetAverage)%n}.inject(:+)
      
      #Update to actual average.
      newLocalAverage  += localSum  + localAverageRest/n
      newOffsetAverage += offsetSum + offsetAverageRest/n
      
      #Reuse temp variables localSum and offsetSum for skew estimate
      localSum          = @buffer.map{|x| (x[:time_received] - newLocalAverage)**2}.inject(:+)
      offsetSum         = @buffer.map{|x| (x[:time_received] - newLocalAverage)*(x[:time_diff] - newOffsetAverage)}.inject(:+)      
      
      @skew             = (localSum != 0) ? (offsetSum.to_f/localSum.to_f) : @skew
      @offsetAverage    = newOffsetAverage
      @localAverage     = newLocalAverage
  end

  def virtual_clock
    error = @clock.time - @localAverage
    error = (error*@skew).to_i
    return (error + @clock.time + @offsetAverage)
  end

  def log
    [@my_id, virtual_clock, (1.0 + @skew), ClockManager.simulation_time]
  end
end

if __FILE__ == $0
end