#  event_management.rb
#  framework

#  Created by Mark Fabbro and Adair Lang, April 2013
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
# 

#
# ClockManager is the class that simulates the local oscillators and virtual
# clocks of the sensor network. It maintains a list of all the clocks running
# at any one time and by the singleton method +set_simulation_time+ it updates
# all the clocks simultaneously based on how much the standardised 'simulation
# clock' has advanced by.
# --
class ClockManager
  @@simulation_clock = 0
  @@local_osc_clocks = []
  @@virtual_clocks   = []

  attr_accessor :skew, :offset, :time

  ##
  # singleton method used to access the current global simulation time
  # --  
  def self.simulation_time
    @@simulation_clock
  end

  ##
  # singleton method used to update the times of every ClockManager instance
  # simultaneously
  # ==== Attributes
  # * +next_simulation_time+ - The new simulation time the client wishes to
  #   advance to. 
  # --
  def self.set_simulation_time(next_simulation_time)
    @@local_osc_clocks.each do |clock|
      clock.advance_time(next_simulation_time - @@simulation_clock)
    end

    # virtual_clocks are dependent on local_osc_clocks; update them after.
    @@virtual_clocks.each do |clock|
      clock.advance_time(next_simulation_time - @@simulation_clock)
    end
    
    # Set new simulation clock time.
    @@simulation_clock = next_simulation_time
  end

  ##
  # singleton method used to restart the clock manager to its base state. Use
  # this between consecutive tests.
  # --
  def self.clean_up
    @@simulation_clock = 0
    @@local_osc_clocks = []
    @@virtual_clocks   = []    
  end

  ##
  # ==== Attributes
  # * +nom_crystal_hz+ - the frequency of the underlying local oscillator
  #   which this clock is derived from.
  # * +skew+   - the rate this clock instance updates its ticks compared to
  #   the simulation clock.
  # * +offset+ - the total number of ticks this clock is initialised with at
  #   simulation time 0.
  # * +drift+  - not currently implemented. In the future will describe how
  #   the skew of the local_oscillator clock types change with time.
  # * +type+   - can either be +:local_osc+ if the clock represents the local
  #   oscillator of a sensor; or, +:virtual+, if the clock instance is tied to
  #   another clock rather than directly to the simulation time.
  # * +guide_clock+ - if +:virtual+ was selected for the +type+ then this
  #   input must include a clock_instance that the virtual clock operates in
  #   relation to.
  # --    
  def initialize(nom_crystal_hz, skew=1, offset=0, drift=0, type=:local_osc, guide_clock=nil)
    @type   = type
    @skew   = skew
    @offset = offset
    @time   = offset
    @drift  = drift
    @nom_crystal_hz = nom_crystal_hz
    case type
    when :local_osc
        @@local_osc_clocks << self
    when :virtual
      @@virtual_clocks << self
      @guide_clock = guide_clock
      @previous_guide_clock_time = @guide_clock.time
    end
  end

  ##
  # The +future_time+ method calculates the time in the future according to
  # the idiosyncrasies of the current clock. 
  # Example. If the simulation clock operates at 1Hz and the local clock has a
  # skew of 0.5, future_time(1000) => 2 + sim_time
  # ==== Attributes
  # * +msec_in_future+ - the number of milliseconds (according to this clock)
  #   the output should be in the future.
  #--       
  def future_time(msec_in_future)
    # Remember, guide_clocks must be local_oscillators.
    skew   = @skew
    skew  *= @guide_clock.skew if @guide_clock
    future = (msec_in_future * (@nom_crystal_hz / 1000))/skew

    future = 1 if future == 0 
    return (@@simulation_clock + future).floor
  end

  ##
  # The +advance_time+ method advances this ClockManager instance's time. If
  # the clock is a local oscillator, the sim_clocktics is used in conjunction
  # with the skew and potentially drift to find the new local oscillator time.
  # If the ClockManager instance is a virtual clock, the virtual clock will
  # advance by the difference between its parent's clock ticks due to the same
  # update. It is known that all virtual clocks will be updated immediately
  # after all local oscillator updates (see self.set_simulation_time)
  # ==== Attributes
  # * +sim_clockticks+ - the number of oscillations the simulation clock has
  #   advanced by.
  #--    
  def advance_time(sim_clockticks)
    case @type
    when :local_osc
      @time += (sim_clockticks*@skew).floor
    when :virtual
      @time +=  ((@guide_clock.time - @previous_guide_clock_time)*@skew).floor
      @previous_guide_clock_time = @guide_clock.time
    end
  end
end

class EventManager
  @@event_magazine = []
  @@new_events     = []


  ##
  # This method returns the simulation time for the next event in the event
  # magazine.
  #--
  def self.next_simulation_time
    # sort events by time (ascending)
    @@event_magazine = @@event_magazine + @@new_events
    @@new_events = []
    @@event_magazine.sort! {|a, b| a[:time] <=> b[:time]} 
    if @@event_magazine.any?
      return @@event_magazine.first[:time]
    else
      return nil
    end
  end

  ##
  # This method returns the EventManager into its original state. Use this
  # between tests.
  # --
  def self.clean_up
    @@event_magazine = []
    @@new_events     = []    
  end
  
  ##
  # ==== Attributes
  # * +client+ - an instance of an object that requires event management.
  # * +clock+  - an instance of a ClockManager object that will be used to
  #   identify when events should be triggered.
  # --
  def initialize(client, clock)
    @client = client
    @clock  = clock
  end

  ##
  # the add_timed_event meethod is used to queue up events that will be
  # triggered upon the expiration of a particular time interval according to
  # the clock associated with this EventManager instance.
  # ==== Attributes
  # * +msec_til_event+   - The time in milliseconds until the event should be
  #   fired.
  # * +event_method+ - The method which will be fired on the @client object
  #   after the event_time has expired.
  # * +method_opts+  - All additional parameters passed to this method will be
  #   passed passed to the +event_method+ when the event is fired on the
  #   +client+.
  # --
  def add_timed_event(msec_til_event, event_method, *method_opts)
    simtime = @clock.future_time(msec_til_event)
    @@new_events << { :time   => simtime, 
                      :client => @client, 
                      :method => event_method,
                      :opts   => method_opts}
  end

  ##
  # the process_events method is a singleton method that should be called each
  # time the simulation clock is updated. This method performs the checks to
  # see if an event should fire due to the current simulation time. 
  # ==== Attributes
  # * +current_time+ - the current simulation time.
  # --
  def self.process_events(current_time)
    @@event_magazine.delete_if do |event|
      if event[:time] <= current_time
        event[:client].send(event[:method], *event[:opts])
        true  # forces delete_if to delete this element
      else
        false # forces delete_if to keep this element
      end
    end
  end

  def self.log
    pp "EventManager_Magazine", @@event_magazine
  end
end
