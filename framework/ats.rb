# 
#  ats.rb
#  simulation
#  
#  Created by Mark Fabbro and Adair Lang, April 2013.
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
# 

require 'csv'
require '../framework/network_manager.rb'
require '../framework/event_management.rb'

class ATSSensorNode
  def initialize(name, nom_crystal_hz, skew, offset, synch_int, rho_eta=0.7, rho_v=0.7, rho_o=0.7, forget_fact=0.999)
    @name             = name
    @my_id            = name.match(/\w+-(\d+)/)[1].to_i
    @clock            = ClockManager.new(nom_crystal_hz, skew, offset, 0)
    @net_manager      = NetworkManager.new(self, name)
    @event_manager    = EventManager.new(self, @clock)
    @ats_data         = {}
    @synch_int        = synch_int
    @rho_eta          = rho_eta
    @rho_v            = rho_v
    @rho_o            = rho_o
    @alpha_hat_i      = 1
    @tau_i_ref        = @clock.time
    @tau_hat_i_ref    = @tau_i_ref
    @delay            = Hash.new
    @delay_gain       = Hash.new
    @forget_fact      = forget_fact
    @delay_estimator_enabled = (forget_fact != nil )
    @event_manager.add_timed_event(@synch_int, :synch_int_expired)
  end
  
  def virtual_clock
    # pp @alpha_hat_i, @clock.time, @tau_i_ref, @tau_hat_i_ref
    (@alpha_hat_i*(@clock.time - @tau_i_ref) + @tau_hat_i_ref).floor
  end
  
  def synch_int_expired
    # Create an ATS Network Packet and send to all neighbours.
    packet = {:name      => @name,      :tau         => @clock.time,
              :tau_ref   => @tau_i_ref, :tau_hat_ref => @tau_hat_i_ref, :alpha_hat => @alpha_hat_i}
    @net_manager.neighbours.each{|n| @net_manager.send_packet(n, Marshal.dump(packet))}
    @event_manager.add_timed_event(@synch_int, :synch_int_expired)
  end

  def recv_packet(mesg)
    mesg = Marshal.load(mesg)
    time_received = @clock.time
    #Determine type of packet that has been received 
    if(mesg[:type]=='delay_int')
      reply_delay_packet(mesg)
      return
    elsif(mesg[:type]=='delay_reply')
      update_delay(mesg,time_received)
      return
    end

    if @delay[mesg[:name]].eql?(nil)
      @delay[mesg[:name]]      = 0.0
      @delay_gain[mesg[:name]] = 1.0
    end

    name_j        = mesg[:name]
    alpha_hat_j   = mesg[:alpha_hat]
    tau_j         = mesg[:tau] + (@alpha_hat_i/alpha_hat_j)*@delay[name_j]
    tau_j_ref     = mesg[:tau_ref]
    tau_hat_j_ref = mesg[:tau_hat_ref]
    tau_ij        = time_received
    tau_hat_i     = virtual_clock

    if @delay_estimator_enabled
      send_delay_packet(name_j)
    end

    if @ats_data[name_j].nil?
      @ats_data[name_j] = {:eta_ij => 1,
                           :tau_j  => tau_j,
                           :tau_ij  => tau_ij}
    else

      delta_tau_j = tau_j - @ats_data[name_j][:tau_j]
      delta_tau_ij = tau_ij - @ats_data[name_j][:tau_ij]
      return if delta_tau_ij == 0 # Protect against divide by zero.

      eta_ij     = @rho_eta*@ats_data[name_j][:eta_ij]+(1 - @rho_eta)*(delta_tau_j.to_f/delta_tau_ij)
      @alpha_hat_i = @rho_v*@alpha_hat_i + (1 - @rho_v)*eta_ij*alpha_hat_j

      @tau_hat_i_ref    = @rho_o * (@alpha_hat_i * (tau_ij - @tau_i_ref) + @tau_hat_i_ref) +
                          (1 - @rho_o)* (alpha_hat_j * (tau_j - tau_j_ref) + tau_hat_j_ref)      
      @tau_i_ref        = tau_ij                    
      @ats_data[name_j] = {:eta_ij => eta_ij,
                           :tau_j  => tau_j,
                           :tau_ij => tau_ij}
    end
  end

  def update_delay(mesg,time_received)
      neigh               = mesg[:name]
	    @delay[neigh]       = @delay[neigh] + @delay_gain[neigh]*((1.0/2.0)*((time_received - mesg[:origin_time_stamp])) - @delay[neigh])
      @delay_gain[neigh]  = @delay_gain[neigh]/(@forget_fact+@delay_gain[neigh])
  end

  def send_delay_packet(destination)
    packet = {:name        => @name,
             :type        => 'delay_int',
             :time_stamp  => @clock.time}
    @net_manager.send_packet(destination,Marshal.dump(packet))
  end
  
  def reply_delay_packet(mesg)
    packet = {:name              => @name,
              :type              => 'delay_reply',
              :origin_time_stamp => mesg[:time_stamp]}
    @net_manager.send_packet(mesg[:name],Marshal.dump(packet))
  end
  
  def log
    [@my_id, virtual_clock, @alpha_hat_i, ClockManager.simulation_time]
  end
end