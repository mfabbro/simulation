# New Log details:
# [node_name as a number, virtual_time, virtual_skew, virtual_error, current_sim_time]

# alg.log will return all but virtual_error. ClockManager needs to provide ensemble average on request.

# virtual_error is distance away from ensemble mean at that sim_time.


def generate_log_data(sensors, log)
  clock_ensemble_mean = (sensors.map{|s| s.log[1]}.inject(:+))/(sensors.length)
  sensors.each do |s|
    log << [s.log[0], s.log[1],  s.log[2],  (s.log[1]-clock_ensemble_mean), s.log[3]]
  end
end