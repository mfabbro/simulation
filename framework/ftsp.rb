#
#  ftsp.rb
#  simulation
#
#  Created by Mark Fabbro and Adair Lang, May 2013.
#  Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
#

require '../framework/network_manager.rb'
require '../framework/event_management.rb'
require '../framework/matrix.rb'

class FTSPSensorNode
  #
  #
  # Assumptions: Assuming that names are always going to be 'node-xxx' where
  # xxx are numbers and unique.

  # root_timeout: If I have send root_timeout number of packets before I
  #receive a packet from the root. I presume the root is dead.

  def initialize(name, nom_crystal_hz, skew, offset, synch_int, initial_buffer_size, root_timeout, lambda=0.999,delay_estimator = true)
    @name             = name
    @my_id            = name.match(/\w+-(\d+)/)[1].to_i
    @clock            = ClockManager.new(nom_crystal_hz, skew, offset, 0)
    @net_manager      = NetworkManager.new(self, name)
    @virtual_clock    = ClockManager.new(nom_crystal_hz, 1, offset, 0, :virtual, @clock)
    @event_manager    = EventManager.new(self, @clock)
    @synch_int        = synch_int
    @broadcast_no     = 0

    # Describe these vars.
    @curr_root_est    = @my_id #the id of who the local node thinks the root node is
    @root_change      = false  #A flag to indicate that the local node thinks the root has changed
    @root_timeout     = root_timeout
    @old_root         = @my_id #id of old root if we take over root so we can ignore redundant old root packets.
    @old_bc_no        = 0 #the last bc_no we received from old root before time-out.
    @delay            = Hash.new #a parameter that keeps track of estimated delay mean between 2 nodes
	   @delay_gain       = Hash.new # a gain parameter to filter new delay estimation.
	   @delay_estimator  = delay_estimator #a boolean variable that specifies whether or not to use delay estimation
    # number of packets we've sent since we have received a packet from the root.
    @broadcasts_since_root = 0
    @initialise            = true
    @initial_buffer_size   = initial_buffer_size
    @forget_fact           = lambda #at the moment the same forgetting factor is used for linear regression for clock params and delay estimation.
    @theta                 = [0,0]
    @P                     = Matrix.identity(2)
    @phi                   = Matrix.column_vector([@clock.time,1])
    @initial_buffer        = {:time_received => [], :time_diff => []}
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end
  def read_my_id
  @my_id
  end
  def broadcast
   # if no new data has arrived with respect to root (in root_timeout syncs)
   # then assume that root has dropped out

    if (@curr_root_est != @my_id) and (@broadcasts_since_root >= @root_timeout)
        @old_root      = @curr_root_est
        @old_bc_no     = @broadcast_no
        @curr_root_est = @my_id
        @root_change   = true
    end
     # Create a FTSP Network Packet and send to all neighbours.
     # TODO: check to make sure virtual_clock.time is actually the 'real'
     # virtual clock time.
    packet = {:name        => @name,
              :type        => 'bc',
              :bc_no       => @broadcast_no,
              :time_stamp  => @virtual_clock.time,
              :root_id     => @curr_root_est}

    @net_manager.neighbours.each do |n|
      @net_manager.send_packet(n, Marshal.dump(packet))
    end

    #root is the only one to increment broadcast_no
    if @curr_root_est == @my_id
        @broadcast_no += 1
    else
        @broadcasts_since_root +=1
    end
    @event_manager.add_timed_event(@synch_int, :broadcast)
  end
  
  def send_delay_packet(destination)
       packet = {:name        => @name,
                 :type        => 'delay_int',
				 :time_stamp  => @clock.time}
	@net_manager.send_packet(destination,Marshal.dump(packet))
  end
  
  def reply_delay_packet(mesg)
    packet = {:name              => @name,
	          :type              => 'delay_reply',
			  :origin_time_stamp => mesg[:time_stamp]}
   @net_manager.send_packet(mesg[:name],Marshal.dump(packet))
  end
  
  def recv_packet(mesg)
    
    mesg = Marshal.load(mesg)
    time_received = @clock.time
    #Determine type of packet that has been received 
	  if(mesg[:type]=='delay_int')
	   reply_delay_packet(mesg)
	   return 
	   elsif(mesg[:type]=='delay_reply')
	   update_delay(mesg,time_received)
	  return 
	end
	  
	#Else It is a 'bc' message that needs to determined if it is relevant.
	redundant_mesg = false
    if mesg[:root_id] < @curr_root_est #is the mesg root younger than our estimate
        if ((mesg[:root_id] == @old_root) && (mesg[:bc_no]== @old_bc_no)) then return end#if it is the old root with old bc_no then ignore.
        @curr_root_est = mesg[:root_id]
        @root_change   = true 
    elsif (@curr_root_est == @my_id) then return
    
    elsif (mesg[:root_id] > @curr_root_est) or (mesg[:bc_no] <= @broadcast_no) #is it older root or is it expected root with old bc_no
    return 

    end

	#If we reach here then we either have a packet from a new root or we have a new packet (bc_no) from the current root.

	#Reset bc_since_root and update bc_number.  
        @broadcasts_since_root  = 0
        @broadcast_no           = mesg[:bc_no]
		#Initialise delay to 0 (note that if delay_estimator is off then delay stays at 0).
		if(@delay[mesg[:name]]==nil)
		  @delay[mesg[:name]]      = 0;
		  @delay_gain[mesg[:name]] = 1;
		end
    
    #Update clock with mesg and time received
		update_clock_params(mesg,time_received)
    
    #If delay estimator is on then send a delay_int packet to update delay estimation with this neighbour.
	    if(@delay_estimator)
		    send_delay_packet(mesg[:name])
		  end
          

  end
  
  def update_delay(mesg,time_received)
      neigh = mesg[:name]
	  @delay[neigh] = @delay[neigh] + @delay_gain[neigh]*((1.0/2.0)*((time_received - mesg[:origin_time_stamp])) - @delay[neigh])
      @delay_gain[neigh]  = @delay_gain[neigh]/(@forget_fact+@delay_gain[neigh])
  end
  
  def update_clock_params(mesg,time_received)   

        time_received = time_received - @delay[mesg[:name]]
        #If Root has changed since last update then need to reinitialise.
        if @root_change 
         @initialise        = true
         @initial_buffer    = {:time_received => [], :time_diff => []}
         @root_change       = false
        end
        
        if @initialise
          @initial_buffer[:time_received] << time_received
          @initial_buffer[:time_diff]     << time_received - mesg[:time_stamp]
          do_initialise_ftsp #Start-up with normal least squares.

        else
          # !!!!!!!!!!!!!!! !%%$$$!%% There is no assign element method in
          # matrix.rb, all I'm doing is assigning @phi[0,0] = time_received.
          @phi = @phi - Matrix[[@phi[0,0]] ,[0]] + Matrix[[time_received] ,[0]]
            @last_measurement = time_received - mesg[:time_stamp]
            do_ftsp #do normal RLS with mesg data
        end
end


  def do_initialise_ftsp
    #only update if there are at least 2 points (need 2 points for linear regression)
    if (@initial_buffer[:time_diff].length) == @initial_buffer_size
      # Do linear regression using Least Squares
      n   = @initial_buffer[:time_diff].length
      sx  = @initial_buffer[:time_received].inject(:+)
      sy  = @initial_buffer[:time_diff].inject(:+)
      sxx = (@initial_buffer[:time_received].map{|x| x ** 2}).inject(:+)
      # Calculate the sum of the products of :time_received and :time_diff.
      sxy  = (@initial_buffer[:time_received].zip(@initial_buffer[:time_diff]).map {|x,y| x*y}).inject(:+)

      # Least Squares Formulae. Performing a linear regression which will is
      # used to adjust your virtual clock.
      @theta[0] = (n*sxy - sx*sy).to_f/(n*sxx - sx*sx)
      @theta[1] = (sy*sxx - sx*sxy).to_f/(n*sxx - sx*sx)
      @P = (Matrix[[n, -sx],[-sx, sxx]])*(1.0/(n*sxx-sx*sx)) 
      
      #If buffer has reached max size then initialise can be turned off
      if(n.eql?(@initial_buffer_size))
      @initialise = false
      end

      update_virt_clock

    else
      # nothing, wait until buffer is has at least @initial_buffer_size values.
    end
  end

  def do_ftsp
    #Use RLS to update parameter estimates.
    error = @last_measurement- @phi[0,0]*@theta[0] - @theta[1]
    @P = (1.0/@forget_fact)*(@P-(@P*@phi*@phi.transpose*@P)*
    (1.0/(@forget_fact+(@phi.transpose*@P*@phi)[0,0])))
    #somehow phi needs to be also matrix type for this to work
    temp = @P*@phi*error #should be a column vector (2 rows).
    @theta[0] = @theta[0] + temp[0,0]
    @theta[1] = @theta[1] + temp[1,0]
    update_virt_clock
  end

  def update_virt_clock
    @virtual_clock.skew = 1-@theta[0]
    #correct clock manually then it can update using it's new skew
    @virtual_clock.time = (@clock.time*@virtual_clock.skew-@theta[1]).round
  end

  def log
    [@my_id, @virtual_clock.time, @virtual_clock.skew, ClockManager.simulation_time]
  end
end

if __FILE__ == $0
end