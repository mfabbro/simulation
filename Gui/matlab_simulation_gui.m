function varargout = matlab_simulation_gui(varargin)
% MATLAB_SIMULATION_GUI MATLAB code for matlab_simulation_gui.fig
%      MATLAB_SIMULATION_GUI, by itself, creates a new MATLAB_SIMULATION_GUI or raises the existing
%      singleton*.
%
%      H = MATLAB_SIMULATION_GUI returns the handle to a new MATLAB_SIMULATION_GUI or the handle to
%      the existing singleton*.
%
%      MATLAB_SIMULATION_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MATLAB_SIMULATION_GUI.M with the given input arguments.
%
%      MATLAB_SIMULATION_GUI('Property','Value',...) creates a new MATLAB_SIMULATION_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before matlab_simulation_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to matlab_simulation_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help matlab_simulation_gui

% Last Modified by GUIDE v2.5 16-Oct-2013 09:58:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @matlab_simulation_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @matlab_simulation_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before matlab_simulation_gui is made visible.
function matlab_simulation_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to matlab_simulation_gui (see VARARGIN)

% Choose default command line output for matlab_simulation_gui
zoom off;
movegui(gcf,'center')
handles.output = hObject;
%set(hObject, 'units','normalized','outerposition',[0 0 1 1]); % ?? Border is still visible?!
%set(h.top_axes,'Position',[350 20 750 700]) 
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes matlab_simulation_gui wait for user response (see UIRESUME)
% uiwait(handles.testfig);

set(handles.pb_back, 'Visible', 'off', 'HitTest', 'off');
pb_delay_none_Callback(hObject, eventdata, handles);
uiwait(hObject);



% --- Outputs from this function are returned to the command line.
function varargout = matlab_simulation_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get default command line output from handles structure
if(isempty(handles))
 varargout{1} = -1
 return 
end
varargout{1} = handles.output;


% --- Executes on button press in pb_gen_topology.
function pb_gen_topology_Callback(hObject, eventdata, handles)
create_network();
    
% --- Executes on button press in pb_run_simulation.
function pb_run_simulation_Callback(hObject, eventdata, handles)
delay      = get(handles.txt_delay_type, 'String');
delay_var1 = get(handles.tf_delay_var1, 'String');
delay_var2 = get(handles.tf_delay_var2, 'String');
delay_comp = get(handles.cb_delay_comp, 'Value');

if delay_comp == 0
    delay_comp = 'off'
else
    delay_comp = 'on'
end

note = sprintf('Starting Ruby Simulation...\n');
set(handles.notification, 'String', note);
drawnow;
['ruby gui_driven_simulation.rb -d ', delay, ' -n ', delay_var1, ' -m ', delay_var2 ' -c', delay_comp]
currdir = pwd;
cd ../test_fixtures/
unix(['ruby gui_driven_simulation.rb -d ', delay, ' -n ', delay_var1, ' -m ', delay_var2, ' -c', delay_comp], '-echo')
cd(currdir);
note = [note, sprintf('Finished Ruby Simulation.')];
drawnow;
set(handles.notification, 'String', note);
bdfuncs= save_plot_bdfuncs(handles);
read_plot_log_files_gui('gui_test', 'ATSSensorNode', [handles.plot_ensemble_curr, handles.plot_ss_curr, handles.plot_ss_prev, handles.plot_ensemble_prev]);
restore_plot_bdfuncs(handles, bdfuncs);

% --- Executes on mouse press over axes background.
function plot_zoom_ButtonDownFcn(hObject, eventdata, handles)
hold(hObject, 'off')
% h1 = title(handles.plot_bigplot, '');
% h2 = xlabel(handles.plot_bigplot, '');
% h3 = ylabel(handles.plot_bigplot, '');
% set(handles.plot_bigplot, 'Title',  h1);
% set(handles.plot_bigplot, 'XLabel', h2);
% set(handles.plot_bigplot, 'YLabel', h3);
axes_copy(hObject, handles.plot_bigplot);

set(handles.plot_bigplot, 'FontSize', 20);
set(get(handles.plot_bigplot, 'Title'),  'Fontsize', 20);
set(get(handles.plot_bigplot, 'XLabel'), 'Fontsize',20);
set(get(handles.plot_bigplot, 'YLabel'), 'Fontsize', 20);
grid(handles.plot_bigplot, 'on');
zoom(0);
zoom on;
set(handles.pb_back, 'Visible', 'on', 'HitTest', 'on');
set_invisibility(handles.pb_back, 'on');
set_invisibility(handles.plot_bigplot, 'on');
set_invisibility(handles.plot_ss_curr, 'off');
set_invisibility(handles.plot_ensemble_curr, 'off');
set_invisibility(handles.plot_ss_prev, 'off');
set_invisibility(handles.plot_ensemble_prev, 'off');


% --- Executes on button press in pb_back.
function pb_back_Callback(hObject, eventdata, handles)
zoom off;
delete(allchild(handles.plot_bigplot));
set_invisibility(handles.pb_back, 'off');
set_invisibility(handles.plot_bigplot, 'off');
set_invisibility(handles.plot_ss_curr, 'on');
set_invisibility(handles.plot_ensemble_curr, 'on');
set_invisibility(handles.plot_ss_prev, 'on');
set_invisibility(handles.plot_ensemble_prev, 'on');


% --- Executes during object creation, after setting all properties.
function tf_delay_var1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function tf_delay_var2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pb_ats_alg.
function pb_ats_alg_Callback(hObject, eventdata, handles)
set(handles.txt_displayed_alg, 'Visible', 'on', 'String', 'Average Time Sync');
pb_back_Callback(hObject, eventdata, handles);
bdfuncs= save_plot_bdfuncs(handles);
read_plot_log_files_gui('gui_test', 'ATSSensorNode', [handles.plot_ensemble_curr, handles.plot_ss_curr, handles.plot_ss_prev, handles.plot_ensemble_prev]);
restore_plot_bdfuncs(handles, bdfuncs);

% --- Executes on button press in pb_carli_alg.
function pb_carli_alg_Callback(hObject, eventdata, handles)
set(handles.txt_displayed_alg, 'Visible', 'on', 'String', 'Carli Linear Consensus');
pb_back_Callback(hObject, eventdata, handles);
bdfuncs= save_plot_bdfuncs(handles);
read_plot_log_files_gui('gui_test', 'CarliSensorNode', [handles.plot_ensemble_curr, handles.plot_ss_curr, handles.plot_ss_prev, handles.plot_ensemble_prev]);
restore_plot_bdfuncs(handles, bdfuncs);

% --- Executes on button press in pb_ftsp_alg.
function pb_ftsp_alg_Callback(hObject, eventdata, handles)
set(handles.txt_displayed_alg, 'Visible', 'on', 'String', 'Flooding Time Sync');
pb_back_Callback(hObject, eventdata, handles);
bdfuncs= save_plot_bdfuncs(handles);
read_plot_log_files_gui('gui_test', 'FTSPSensorNodev2', [handles.plot_ensemble_curr, handles.plot_ss_curr, handles.plot_ss_prev, handles.plot_ensemble_prev]);
restore_plot_bdfuncs(handles, bdfuncs);

% --- Executes on button press in pb_kmr_alg.
function pb_kmr_alg_Callback(hObject, eventdata, handles)
set(handles.txt_displayed_alg, 'Visible', 'on', 'String', 'Kumar Spatial Smoothing');
pb_back_Callback(hObject, eventdata, handles);
bdfuncs= save_plot_bdfuncs(handles);
read_plot_log_files_gui('gui_test', 'KumarSensorNode', [handles.plot_ensemble_curr, handles.plot_ss_curr, handles.plot_ss_prev, handles.plot_ensemble_prev]);
restore_plot_bdfuncs(handles, bdfuncs);


% --- Executes on button press in pb_delay_none.
function pb_delay_none_Callback(hObject, eventdata, handles)
set(handles.txt_delay_type, 'Visible', 'on', 'String', 'static');
set(handles.txt_delay_var1, 'Visible', 'on', 'String', 'Value:');
set(handles.txt_delay_var2, 'Visible', 'off', 'String', 'StdDev:');
set(handles.tf_delay_var1, 'Visible', 'on', 'String', '0');
set(handles.tf_delay_var2, 'Visible', 'off', 'String', '0');


% --- Executes on button press in pb_delay_static.
function pb_delay_static_Callback(hObject, eventdata, handles)
set(handles.txt_delay_type, 'Visible', 'on', 'String', 'static');
set(handles.txt_delay_var1, 'Visible', 'on', 'String', 'Value:');
set(handles.txt_delay_var2, 'Visible', 'off', 'String', 'Value:');
set(handles.tf_delay_var1, 'Visible', 'on', 'String', '4');
set(handles.tf_delay_var2, 'Visible', 'off', 'String', 'Mean');


% --- Executes on button press in pb_delay_uniform.
function pb_delay_uniform_Callback(hObject, eventdata, handles)
set(handles.txt_delay_type, 'Visible', 'on', 'String', 'uniform');
set(handles.txt_delay_var1, 'Visible', 'on', 'String', 'Lower Bound:');
set(handles.txt_delay_var2, 'Visible', 'on', 'String', 'Upper Bound:');
set(handles.tf_delay_var1, 'Visible', 'on', 'String', '4');
set(handles.tf_delay_var2, 'Visible', 'on', 'String', '6');


% --- Executes on button press in pb_delay_normal.
function pb_delay_normal_Callback(hObject, eventdata, handles)
set(handles.txt_delay_type, 'Visible', 'on', 'String', 'normal');
set(handles.txt_delay_var1, 'Visible', 'on', 'String', 'Mean:');
set(handles.txt_delay_var2, 'Visible', 'on', 'String', 'StdDev:');
set(handles.tf_delay_var1, 'Visible', 'on', 'String', '4.3536264');
set(handles.tf_delay_var2, 'Visible', 'on', 'String', '6.7319259');


function bdfuncs = save_plot_bdfuncs(handles)
    bdfunc_plot_ss_curr = get(handles.plot_ss_curr, 'ButtonDownFcn');
    bdfunc_plot_ensemble_curr = get(handles.plot_ensemble_curr, 'ButtonDownFcn');
    bdfunc_plot_ss_prev = get(handles.plot_ss_prev, 'ButtonDownFcn');
    bdfunc_plot_ensemble_prev = get(handles.plot_ensemble_prev, 'ButtonDownFcn');
    bdfuncs = {bdfunc_plot_ss_curr, bdfunc_plot_ensemble_curr, bdfunc_plot_ss_prev, bdfunc_plot_ensemble_prev};

function restore_plot_bdfuncs(handles, bdfuncs)
    set(handles.plot_ss_curr, 'Tag', 'plot_ss_curr', 'ButtonDownFcn', bdfuncs{1});
    set(handles.plot_ensemble_curr, 'Tag', 'plot_ensemble_curr', 'ButtonDownFcn', bdfuncs{2});
    set(handles.plot_ss_prev, 'Tag', 'plot_ss_prev', 'ButtonDownFcn', bdfuncs{3});
    set(handles.plot_ensemble_prev, 'Tag', 'plot_ensemble_prev', 'ButtonDownFcn', bdfuncs{4});


function set_invisibility(hObject, onoff)
    set(hObject, 'Visible', onoff, 'HitTest', onoff);
    set(allchild(hObject), 'Visible', onoff, 'HitTest', onoff);

function axes_copy(axesSource, axesDest)
    copyobj(allchild(axesSource), axesDest);        
    dont_copy_list = {'Type', 'Title', 'XLabel', 'YLabel', 'ZLabel','Children','TightInset','CurrentPoint',...
        'OuterPosition', 'Position', 'BeingDeleted','ButtonDownFcn',...
        'CreateFcn','DeleteFcn','BusyAction', 'HandleVisibility','HitTest',...
        'Interruptible','Parent','Selected', 'SelectionHighlight','Tag',...
        'UIContextMenu','UserData','Visible'};
    axes_props = get(axesSource);
    for nc = 1:length(dont_copy_list);
        axes_props = rmfield(axes_props, dont_copy_list{nc});
    end
    set(axesDest, axes_props);



function tf_delay_var1_Callback(hObject, eventdata, handles)
% Hints: get(hObject,'String') returns contents of tf_delay_var1 as text
%        str2double(get(hObject,'String')) returns contents of tf_delay_var1 as a double
val = str2num(get(hObject, 'String'));
if (isempty(val) || val < 0)
    set(hObject, 'String', '0');
end



function tf_delay_var2_Callback(hObject, eventdata, handles)
% Hints: get(hObject,'String') returns contents of tf_delay_var1 as text
%        str2double(get(hObject,'String')) returns contents of tf_delay_var1 as a double
val = str2num(get(hObject, 'String'));
if (isempty(val) || val < 0)
    set(hObject, 'String', '0');
end


% --- Executes on button press in cb_delay_comp.
function cb_delay_comp_Callback(hObject, eventdata, handles)
% hObject    handle to cb_delay_comp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_delay_comp


% --- Executes when user attempts to close testfig.
function testfig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to testfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(hObject);
% Hint: delete(hObject) closes the figure
delete(hObject);
