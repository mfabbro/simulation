function varargout = create_network(varargin)
% CREATE_NETWORK MATLAB code for create_network.fig
%      CREATE_NETWORK, by itself, creates a new CREATE_NETWORK or raises the existing
%      singleton*.
%
%      H = CREATE_NETWORK returns the handle to a new CREATE_NETWORK or the handle to
%      the existing singleton*.
%
%      CREATE_NETWORK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATE_NETWORK.M with the given input arguments.
%
%      CREATE_NETWORK('Property','Value',...) creates a new CREATE_NETWORK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before create_network_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to create_network_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help create_network

% Last Modified by GUIDE v2.5 12-Oct-2013 16:15:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @create_network_OpeningFcn, ...
                   'gui_OutputFcn',  @create_network_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before create_network is made visible.
function create_network_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to create_topology (see VARARGIN)


        h = handles;
%h.main = varargin{1}.topology_fig;
%        h.parent  = varargin{1};
        h.prevIdx = [];
        h.selectIdx = [];
        set(h.top_axes,'XLim',[0 1], 'YLim',[0 1], 'XTick',[], 'YTick',[], 'Box','on', ...
            'Units','pixels');
        h.pts = line(NaN, NaN, 'Parent',h.top_axes, 'HitTest','off', ...list
            'Marker','o', 'MarkerSize',10, 'MarkerFaceColor','b', ...
            'LineStyle','none');
        h.root = line(NaN, NaN, 'Parent',h.top_axes, 'HitTest','off', ...list
            'Marker','o', 'MarkerSize',10, 'MarkerFaceColor','r', ...
            'LineStyle','none');
        h.selected = line(NaN, NaN, 'Parent',h.top_axes, 'HitTest','off', ...
            'Marker','o', 'MarkerSize',10, 'MarkerFaceColor','y', ...
            'LineStyle','none');
        h.prev = line(NaN, NaN, 'Parent',h.top_axes, 'HitTest','off', ...
            'Marker','o', 'MarkerSize',20, 'Color','r', ...
            'LineStyle','none', 'LineWidth',2);
        h.edges = line(NaN, NaN, 'Parent',h.top_axes, 'HitTest','off', ...
            'LineWidth',2, 'Color','g');
        h.txt = [];
        h = circle_layout(h);
        h.adj_matrix = zeros(16);
        h = redraw(h);
        h.dist_p = 1;
        h.conn_prob = 1;
        set(h.sub_complete,'String','<html><center>Completely Connect<br><center>Selected Nodes');

% Choose default command line output for create_topology
        h.output = 0;
        movegui(gcf,'center')
    %   set(hObject, 'units','normalized','outerposition',[0 0 1 1]); % ?? Border is still visible?!
     %   set(h.top_axes,'Position',[350 20 750 700]) 
        %set(hObject,'outerposition', pos); % Taskbar is visible

% Update handles structure
guidata(hObject, h);
% uiwait(handles.topology_fig);
% This sets up the initial plot - only do when we are invisible
% so window can get raised using create_topology.
if strcmp(get(hObject,'Visible'),'off')
    
end

% UIWAIT makes create_topology wait for user response (see UIRESUME)
uiwait(h.topology_fig);


% --- Outputs from this function are returned to the command line.
function varargout = create_network_OutputFcn(hObject, eventdata, h) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if(isempty(h))
    varargout{1} = -1;
    return
end
for i = 1 : 2
csvwrite(['../test_fixtures/gui_tops/top_' num2str(i) '.csv'],h.adj_m(:,:,i));
end


varargout{1} = h.output;
varargout{2} = h.adj_matrix;
varargout{3} = h.pts_array;
%varargout{2} = copyobj(h.top_axes,h.parent);
delete(hObject);


% --- Executes on selection change in grid_types.
function grid_types_Callback(hObject, eventdata, handles)
% hObject    handle to grid_types (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
popup_sel_index = get(handles.grid_types, 'Value');
switch popup_sel_index
    case 1 %circle layout
        handles = circle_layout(handles);
        guidata(handles.topology_fig,handles);
    case 2 %Grid Layout
        handles = grid_layout(handles);
        guidata(handles.topology_fig,handles);
    case 3 %Disjoint Layout
        handles = disjoint_layout(handles);
        guidata(handles.topology_fig,handles);
    case 4 %Random Layout
        handles = random_layout(handles);
        guidata(handles.topology_fig,handles);
end
h = redraw(handles);
guidata(handles.topology_fig,h);
% Hints: contents = cellstr(get(hObject,'String')) returns grid_types contents as cell array
%        contents{get(hObject,'Value')} returns selected item from grid_types


% --- Executes during object creation, after setting all properties.
function grid_types_CreateFcn(hObject, eventdata, handles)
% hObject    handle to grid_types (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in nodes_list.
function nodes_list_Callback(hObject, eventdata, handles)
% hObject    handle to nodes_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
        handles.selectIdx = get(handles.nodes_list, 'Value');
        h = redraw(handles);
        guidata(handles.topology_fig,h);
% Hints: contents = cellstr(get(hObject,'String')) returns nodes_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from nodes_list


% --- Executes during object creation, after setting all properties.
function nodes_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nodes_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = handles;
h.output = h.output + 1;
temp = h.adj_matrix + h.adj_matrix';
h.adj_m(:,:,h.output) = full(temp);
guidata(h.topology_fig,h);
if(h.output==1)
    choice = questdlg('Would you like to create a second topologoy?', ...
        'Topologoy Confirmation', ...
        'Yes','No','No');
    % Handle response
    switch choice
        case 'Yes'
            set(h.status_text,'String','One Topologoy Saved ');
            %TODO---Add in save for figures.
            clear_all_Callback(h.clear_node, [], h);
            h = guidata(h.topology_fig);
            h = circle_layout(h);
            h = redraw(h);
            guidata(h.topology_fig,h);
        case 'No'
          h.adj_m(:,:,2) = full(temp);
          guidata(h.topology_fig,h);
           uiresume(h.topology_fig);
    end
else
    uiresume(h.topology_fig);
end

% --- Executes on button press in clear_all.
function clear_all_Callback(hObject, eventdata, handles)
% hObject    handle to clear_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.adj_matrix = sparse(zeros(16));
h = redraw(handles);
guidata(handles.topology_fig,h);

% --- Executes on button press in clear_node.
function clear_node_Callback(hObject, eventdata, handles)
% hObject    handle to clear_node (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)       
    h = handles;
    % delete selected node
    idx = get(h.nodes_list, 'Value');        
    %pts(idx,:) = [];
    h.adj_matrix(:,idx) = zeros(size(h.adj_matrix(:,idx)));
    h.adj_matrix(idx,:) = zeros(size(h.adj_matrix(idx,:)));
    h = redraw(h);
    guidata(handles.topology_fig,h);

% --- Executes on button press in update.
function update_Callback(hObject, eventdata, handles)
% hObject    handle to update (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA).
popup_sel_index = get(handles.grid_types, 'Value');
switch popup_sel_index
    case 1 %circle layout
        handles = circle_layout(handles);
        guidata(handles.topology_fig,handles);
    case 2 %Grid Layout
        handles = grid_layout(handles);
        guidata(handles.topology_fig,handles);
    case 3 %Disjoint Layout
        handles = disjoint_layout(handles);
        guidata(handles.topology_fig,handles);
    case 4 %Random Layout
        handles = random_layout(handles);
        guidata(handles.topology_fig,handles);
end
h = redraw(handles);
guidata(handles.topology_fig,h);

function handles = redraw(h)

        % edges
        p = nan(3*nnz(h.adj_matrix),2);
        [i,j] = find(h.adj_matrix);

        p(1:3:end,:) = h.pts_array(i,:);
        p(2:3:end,:) = h.pts_array(j,:);
        set(h.edges, 'XData',p(:,1), 'YData',p(:,2))

        % nodes
        set(h.pts, 'XData',h.pts_array(:,1), 'YData',h.pts_array(:,2))
        set(h.root, 'XData',h.pts_array(1,1), 'YData',h.pts_array(1,2))
        set(h.prev, 'XData',h.pts_array(h.prevIdx,1), 'YData',h.pts_array(h.prevIdx,2))
        set(h.selected, 'XData',h.pts_array(h.selectIdx,1), 'YData',h.pts_array(h.selectIdx,2))

        % list of nodes
        set(h.nodes_list, 'String',num2str([(1:size(h.pts_array,1))'],'Node - %2d '))

        % node labels
        if ishghandle(h.txt), delete(h.txt); end
            h.txt = text(h.pts_array(:,1)+0.01, h.pts_array(:,2)+0.01, ...
                num2str((1:size(h.pts_array,1))'), ...
                'HitTest','off', 'FontSize',12, ...
                'VerticalAlign','bottom', 'HorizontalAlign','left');


        % force refresh
        handles = h;
        drawnow


% --- Executes during object creation, after setting all properties.
function topology_fig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to topology_fig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


function h = circle_layout(h)
        testpts = 0:(2*pi)/16:(2*pi-pi/8);
        h.pts_array = [0.5+0.49*cos(testpts)' 0.5+0.49*sin(testpts)'];

        
function h = grid_layout(h)
        testpts = linspace(0.1,0.9,4);
        for i = 1:4
            h.pts_array((i-1)*4+1:i*4,:) = [testpts' testpts(i)*ones(4,1)];
        end
        
function h = disjoint_layout(h)
        testpts = 0:(2*pi)/8:(2*pi-pi/4);
        h.pts_array = [0.25+0.25*cos(testpts)' 0.25+0.25*sin(testpts)'
                       0.75+0.25*cos(testpts)' 0.75+0.25*sin(testpts)']; 
                   

                   
function h = complete_selection(h)
       if(length(h.selectIdx)<2), return; end
       indexs = combntns(h.selectIdx,2);
       for i = 1:length(indexs)
           h.adj_matrix(indexs(i,1),indexs(i,2)) = 1;
       end
       
       
function h = random_layout(h)
      h.pts_array=rand(16,2);
      
function h = distance_based_connection(h,max_dist)
    n = 16;
    adj=zeros(n,n);
    for node_no=1:n
        for other_no=node_no+1:n
            dist=norm(h.pts_array(node_no,:)-h.pts_array(other_no,:));
            if (dist<max_dist)
                adj(node_no,other_no)=1;
            end
        end
    end
    h.adj_matrix =adj;

        
function h = grid_adj(h)
    %This generates an adjancey matrix for a regular n by n grid with
    %vertical and horizontal connections (4 if not on an edge).
r=4;
c=4;
%mat = [1 2 3; 4 5 6; 7 8 9];              %# Sample matrix
%[r,c] = size(mat);                        %# Get the matrix size
diagVec1 = repmat([ones(c-1,1); 0],r,1);  %# Make the first diagonal vector
                                          %#   (for horizontal connections)
diagVec1 = diagVec1(1:end-1);             %# Remove the last value
diagVec2 = ones(c*(r-1),1);               %# Make the second diagonal vector
                                          %#   (for vertical connections)
h.adj_matrix = diag(diagVec1,1)+...                %# Add the diagonals to a zero matrix
                 diag(diagVec2,c);

       

% --- Executes on mouse press over axes background.
function top_axes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to top_axes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = handles;
%Grab current point     
p = get(h.top_axes, 'CurrentPoint');
   
%Get min distance to a node
[dst,idx] = min(sum(bsxfun(@minus, h.pts_array, p(1,1:2)).^2,2));
    if sqrt(dst) > 0.025, return; end

    if isempty(h.prevIdx)
        % starting node (requires a second click to finish)
        h.prevIdx = idx;
    else
        % add the new edge
        if (idx == h.prevIdx)
            h.prevIdx = [];
        
        elseif(h.adj_matrix(h.prevIdx,idx)||h.adj_matrix(idx,h.prevIdx))
            h.adj_matrix(h.prevIdx,idx) = 0;
            h.adj_matrix(idx,h.prevIdx) = 0;
        else
            h.adj_matrix(h.prevIdx,idx) = 1;
        end
        h.prevIdx = [];
    end
handles1 = redraw(h);
guidata(handles1.topology_fig,handles1);


% --- Executes on button press in sub_complete.
function sub_complete_Callback(hObject, eventdata, handles)
% hObject    handle to sub_complete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = complete_selection(handles);
h = redraw(h);
guidata(h.topology_fig,h);



function distance_text_Callback(hObject, eventdata, handles)
% hObject    handle to distance_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
        input = get(hObject,'String');
            
        [z status] = str2num(input);
        if(status)% string to number
            if(z<0)
                val = 0;
            elseif(z>2)
                val = 2;
            else 
                val = z;
            end
        else
            val = handles.dist_p;
        end
        handles.dist_p = val;
        set(hObject,'String',num2str(handles.dist_p));
        guidata(handles.topology_fig,handles);
% Hints: get(hObject,'String') returns contents of distance_text as text
%        str2double(get(hObject,'String')) returns contents of distance_text as a double
function h = random_connection(h)
        n = 16;
        adj = zeros(n);
        for i=1:n
            for j=i+1:n
                if rand<=h.conn_prob; adj(i,j)=1; end
            end
        end
        h.adj_matrix = adj;

% --- Executes during object creation, after setting all properties.
function distance_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to distance_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in distance_connect.
function distance_connect_Callback(hObject, eventdata, h)
% hObject    handle to distance_connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = distance_based_connection(h,h.dist_p);
h = redraw(h);
guidata(h.topology_fig,h);



function prob_Callback(hObject, eventdata, handles)
% hObject    handle to prob (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
        input = get(hObject,'String');
            
        [z status] = str2num(input);
        if(status && z>=0 && z <= 1)% string to number
            val = z;
        else
            val = handles.conn_prob;
        end
        handles.conn_prob = val;
        set(hObject,'String',num2str(handles.conn_prob));
        guidata(handles.topology_fig,handles);
% Hints: get(hObject,'String') returns contents of prob as text
%        str2double(get(hObject,'String')) returns contents of prob as a double


% --- Executes during object creation, after setting all properties.
function prob_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prob (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in connect_prob.
function connect_prob_Callback(hObject, eventdata, handles)
% hObject    handle to connect_prob (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = random_connection(handles);
h = redraw(h);
guidata(h.topology_fig,h);


% --- Executes on button press in grid_connect.
function grid_connect_Callback(hObject, eventdata, handles)
% hObject    handle to grid_connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 h = grid_adj(handles);
 h = redraw(h);
 guidata(h.topology_fig,h);

% --- Executes on button press in complete_all_btn.
function complete_all_btn_Callback(hObject, eventdata, handles)
% hObject    handle to complete_all_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 handles.adj_matrix = triu(ones(16),1);
 h = redraw(handles);
 guidata(h.topology_fig,h);
