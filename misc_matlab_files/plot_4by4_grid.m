%%

%This generates an adjancey matrix for a regular n by n grid with
%vertical and horizontal connections (4 if not on an edge).
r=4;
c=4;
diagVec1 = repmat([ones(c-1,1); 0],r,1);  %# Make the first diagonal vector
%#   (for horizontal connections)
diagVec1 = diagVec1(1:end-1);             %# Remove the last value
diagVec2 = ones(c*(r-1),1);               %# Make the second diagonal vector
%#   (for vertical connections)
adj_matrix = diag(diagVec1,1)+...                %# Add the diagonals to a zero matrix
    diag(diagVec2,c);

%This generates a series of points for a evenly spaced 4 by 4 grid
testpts = linspace(0.1,0.9,4);
for i = 1:4
    pts_array((i-1)*4+1:i*4,:) = [testpts' testpts(i)*ones(4,1)];
end


%This plots the grid network.
close all

h = figure(1);
axes1 = axes();

set(axes1,'XLim',[0 1], 'YLim',[0 1], 'XTick',[], 'YTick',[], 'Box','on', ...
    'Units','pixels');
pts = line(NaN, NaN,  'Marker','o', 'MarkerSize',10, 'MarkerFaceColor','y', ...
    'LineStyle','none');
root = line(NaN, NaN, 'Marker','o', 'MarkerSize',10, 'MarkerFaceColor','r', ...
    'LineStyle','none');
edges = line(NaN, NaN,  'LineWidth',2, 'Color','k');
txt = [];


%
p = nan(3*nnz(adj_matrix),2);
[i,j] = find(adj_matrix);

p(1:3:end,:) = pts_array(i,:);
p(2:3:end,:) = pts_array(j,:);
set(edges, 'XData',p(:,1), 'YData',p(:,2))

% nodes
set(pts, 'XData',pts_array(:,1), 'YData',pts_array(:,2))
set(root, 'XData',pts_array(1,1), 'YData',pts_array(1,2))
txt = text(pts_array(:,1)+0.01, pts_array(:,2)+0.01, ...
    num2str((1:size(pts_array,1))'), ...
    'HitTest','off', 'FontSize',12, ...
    'VerticalAlign','bottom', 'HorizontalAlign','left');
axis square