function [X Ad K average_skews converge_skews settling_time steady_error end_err] = carli_model_s( no_nodes,offset_range,skew_range,no_updates,T,F,delta,varargin )
%CARLI_MODEL_S is a simple implementation of the linear system describing
%   the synchronous Carli's Linear Concensus Algorithm (CLCA).
%   [X AD K average_skews converge_skews settling_time steady_error end_err] = CARLI_MODEL_S(NO_NODES,OFFSET_RANGE, SKEW_RANGE, NO_UPDATES,T,F,VARARGIN)
%   Calculates the state trajectories X of the linear system of NO_NODES
%   over NO_UPDATES each with a period of T. It Plots a range of outputs
%   associated with these state vectors X.
%   OFFSET_RANGE = [MIN_OFFSET MAX_OFFSET] and SKEW_RANGE = [MIN_SKEW
%   MAX_SKEW]
%   VARARGIN can specify optional arguements in the following order
%   IC_DIST - a string to specify initial condition distrubution
%   (random or linearly spaced) 'random', and 'linear' respectivley
%   Random is assumed by default 
%   ADJ - an adjanceny matrix for the network - must be of size NO_NODES by NO_NODES 
%   A complete graph is assumed if ADJ is not specified. 

if(nargin<7)
    error('carli_s:argChk','carli_model_s:Not Enough Inputs');
elseif (nargin == 7)
    ic_dist = 'random';
    network = 'complete';
elseif (nargin == 8)
    ic_dist = varargin{1};
    network = 'complete';
elseif (nargin == 9)
    ic_dist = varargin{1};
    network = 'standard';
    adj     = varargin{2};
    if(size(adj,1)~= no_nodes || size(adj,2)~=no_nodes)
        error('carli_s:adj_check','carli_model_s: Adj incorrect dimensions should be %d by %d',no_nodes,no_nodes)
    end
else
    error('carli_s:argChk2','carli_model_s:Too Many Inputs')
    
end

%Define min, max of ranges.
min_skew   = skew_range(1);
max_skew   = skew_range(2);
min_offset = offset_range(1);
max_offset = offset_range(2);

%Create Initial Vectors
if(strcmp(ic_dist,'random'))
    offset     = round(min_offset+(max_offset-min_offset).*rand(no_nodes,1));
    skews      = min_skew+(max_skew-min_skew)*rand(no_nodes,1);
elseif(strcmp(ic_dist,'linear'))
    offset     = round(linspace(min_offset,max_offset,no_nodes))';
    skews      = linspace(min_skew,max_skew,no_nodes)';
else 
    error('carli_s:ic_dist','carli_model_s: Invalid ic_dist')
end

%Skews vector
average_skews = mean(skews);
D             = diag(skews);

%Intial Conditions
initial_con   = [offset ;ones(no_nodes,1)];

%Define some useful matrices to create augmented system
I             = eye(no_nodes);
Zero          = zeros(no_nodes);
K             = zeros(no_nodes);
%Calculate K Matrix
if(strcmp(network,'complete'))
    P              = (1/no_nodes)*ones(no_nodes);
    P(logical(I))  = 1-sum(P(2:end,1));
    K              = I-P;
else %Compute K matrix using metropolis method for given adj.
    no_neighs      = sum(adj);
    for i = 1 : no_nodes
        for j = (i+1) : no_nodes
            if(adj(i,j))
                K(i,j) = -1/(max([no_neighs(i);no_neighs(j)]));
            else
                K(i,j) = 0;
            end
        end
    end
    K = K+K';
    K(logical(I)) = -sum(K,2);
end

%Define Ad as given in CLCA
Ad       =  [I T*D;
             Zero I]*([I Zero;Zero I]-[F(1)*K Zero;F(2)*K Zero]);
%Initialise x and pre allocate
X                  =  zeros(2*no_nodes,no_updates);
X(:,1)             =  initial_con;
err                =  zeros(no_nodes,no_updates);
err(:,1)           =  offset-mean(offset);
nominal_skews      =  zeros(no_nodes,no_updates);
nominal_skews(:,1) =  skews;
steady_state       =  0;
%Time marching Loop
for h=1:no_updates
    X(:,h+1)             = Ad*X(:,h);
    av                   = mean(X(1:no_nodes,h+1));
    err(:,h+1)           = X(1:no_nodes,h+1)-av;
    %X(1:no_nodes,h+1)    = floor(X(1:no_nodes,h+1));
    nominal_skews(:,h+1) = X(no_nodes+1:end,h+1).*skews;
    if((h+1>5) && ~steady_state)
       mean_err          = mean(abs(err(:,h-4:h+1)));
       range             = max((err(:,h-4:h+1)))-min((err(:,h-4:h+1)));
       if((std(range)<delta))
           steady_state  = 1;
           settling_time = h+1;
           steady_error  = mean(mean_err);
       end
    end
end
converge_skews = mean(X(no_nodes+1:end,end).*skews);
end_err        = mean(abs(err(:,end)));
%Plots
figure(1)
plot(err')
xlabel('h')
ylabel('e(h)')
title(['Clock errors vs update intervals of period ' num2str(T)])
if(steady_state)
hold on
plot(steady_error*ones(no_updates,1),'--r','LineWidth',2)
plot(-steady_error*ones(no_updates,1),'--g','LineWidth',2)
hold off
else
    fprintf(2,'Error has not converged into specified range')
end
figure(2)
plot(X(1:no_nodes,1:settling_time)')
xlabel('h')
ylabel('\tau_i(hT)')
title(['Virtual Clocks vs update intervals of period ' num2str(T)])
figure(3)
plot(X(no_nodes+1:end,:)')
xlabel('h')
ylabel('\alpha_i(hT)')
title(['Virtual Skews vs update intervals of period ' num2str(T)])
figure(4)
plot(nominal_skews')
xlabel('h')
ylabel('\alpha_i(hT)')
title(['Virtual Skews in reference to nominal time vs update intervals of period ' num2str(T)])

end

