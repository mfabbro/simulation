%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2013 - Capstone Project MC02 - Clock Synchronisation
%
% Created by Mark Fabbro and Adair Lang, August 2013.
% Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
%
% Carli's Linear Consensus Algorithm
%
% Description: A matlab implementation of the completly synchronous version
% of the CLCA.
% Also investigation into the effect of the gain parameter.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function carli_matlab_test(test_id)
global NO_NODES
global T
global NO_UPDATES
global skew_range
global offset_range
global F
global network
%Test 1-  10 Nodes, 100s T and normal delta range, offset range and F
if(strcmp(test_id,'Test1'))
    
    NO_NODES     = 10;
    network      = 'complete';
    ic_dist      = 'linear';
    T            = 100;
    NO_UPDATES   = 100;
    skew_range   = [0.1 6.16];
    offset_range = [-T/10 T/10];
    F            = [1/2 1/(2*T)];
    delta        = 1;
    
    [ x Ad K average_skews converge_skews settling_time steady_error end_error] = carli_model_s( NO_NODES,offset_range,skew_range,NO_UPDATES,T,F,delta,ic_dist );
    stand_eig = carli_cost_fun(F);
    lambda_N  = max(abs(eig(K)));
    
    %Test 2- same as test 1 except optimal F is attempted to be calculated.
elseif(strcmp(test_id,'Test2'))
    NO_NODES     = 10;
    network      = 'complete';
    ic_dist      = 'linear';
    T            = 100;
    NO_UPDATES   = 100;
    skew_range   = [0.1 1.99];
    offset_range = [-T/10 T/10];
    x0           = [1/2 1/(2*T)];
    delta        = 1;
    %adj = grid_adj(4)
    lb=[0; 0];%Lower Bound Vector
    ub=Inf;%upper bound vector
    A=[];%[2 T];% Restiction matix
    b=[];%[4];
    %x0 = [1/2 ; 1/(2*T)];%[1.6530 6.9279e-005];
    mm_eig = 1;
    for l=1:10
        x0(2) = 1/(4*T)+l*1/(8*T);
        for m = 1:10
            x0(1)=m*0.1;
            [F max_eig exitflag]=fmincon(@carli_cost_fun,x0,A,b,[],[],lb,ub);
            if (max_eig<mm_eig)
                bestF  = F;
                mm_eig = max_eig;
            end
        end
    end
    F = bestF;
    [ x Ad K average_skews converge_skews settling_time steady_error end_error] = carli_model_s( NO_NODES,offset_range,skew_range,NO_UPDATES,T,F,delta,ic_dist );
    stand_eig = carli_cost_fun(F);
    lambda_N  = max(abs(eig(K)));
    
    %Test3 determines when the standard conditions of Test1 break. i.e max
    %skew.
elseif(strcmp(test_id,'Test3'))
    NO_NODES     = 10;
    network      = 'complete';
    ic_dist      = 'linear';
    T            = 100;
    NO_UPDATES   = 100;
    skew_range   = [0.1 1.99];
    offset_range = [-T/10 T/10];
    x0           = [1/2 1/(2*T)];
    delta        = 1;
    stable       = 1;
    while(stable)
        stand_eig = carli_cost_fun(F);
        if(stand_eig>=1)
            stable = 0;
            max_skew = skew_range(2);
        else
            skew_range(2)=skew_range(2)+0.01;
        end
    end
elseif(strcmp(test_id,'Test4'))
    NO_NODES     = 25;
    network      = 'grid';
    ic_dist      = 'linear';
    T            = 100;
    NO_UPDATES   = 200;
    skew_range   = [0.1 3.1];
    offset_range = [-T/10 T/10];
    F           = [1/2 1/(2*T)];
    delta        = 1;
    stable       = 1;
    adj = grid_adj(5);
   [ x Ad K average_skews converge_skews settling_time steady_error end_error] = carli_model_s( NO_NODES,offset_range,skew_range,NO_UPDATES,T,F,delta,ic_dist ,adj);
    stand_eig = carli_cost_fun(F);
    lambda_N  = max(abs(eig(K)));
end
print_carli_results();
if(~strcmp(test_id,'Test3'))
for fig_no = 1:4
    print2eps([test_id '_fig_no' num2str(fig_no)],fig_no)
end
else 
end


    function print_carli_results()
        fprintf(1,'For %s\n',test_id)
        fprintf(1,'------------------------------------\n')
        if(strcmp(test_id,'Test3'))
        fprintf(1,'Maximium skew for standard test : %5.2f\n',max_skew)
        else
        fprintf(1,'Maximium eigenvalue of Ad <1 : %5.4f\n',stand_eig)
        fprintf(1,'Maximium eigenvalue of K ~=0 : %5.4f\n',lambda_N)
        fprintf(1,'Average of initial skews is  : %5.4f\n',average_skews)
        fprintf(1,'Average of converged skews is: %5.4f\n',converge_skews)
        fprintf(1,'Settling time                : %5d\n',settling_time)
        fprintf(1,'Error at settling time       : %5.4f\n',steady_error)
        fprintf(1,'Error at end time            : %5.4e\n',end_error)
        end
        
    end


    function [ lambda ] = carli_cost_fun( F )
        
        
        no_nodes   = NO_NODES;
        min_skew   = skew_range(1);
        max_skew   = skew_range(2);
        min_offset = offset_range(1);
        max_offset = offset_range(2);
        
        offset     = round(linspace(min_offset,max_offset,no_nodes))';
        skews      = linspace(min_skew,max_skew,no_nodes)';
        
        I    = eye(no_nodes);
        Zero = zeros(no_nodes);
        D    = diag(skews);
        K1   = zeros(no_nodes);
        if(strcmp(network,'complete'))
            P              = (1/no_nodes)*ones(no_nodes);
            P(logical(I))  = 1-sum(P(2:end,1));
            K1              = I-P;
        else %Compute K matrix using metropolis method for given adj.
            no_neighs      = sum(adj);
            for i = 1 : no_nodes
                for j = (i+1) : no_nodes
                    if(adj(i,j))
                        K1(i,j) = -1/(max([no_neighs(i);no_neighs(j)]));
                    else
                        K1(i,j) = 0;
                    end
                end
            end
            K1 = K1+K1';
            K1(logical(I)) = -sum(K1,2);
        end
        
        %Define Ad as given in CLCA
        Ad1       =  [I T*D;
            Zero I]*([I Zero;Zero I]-[F(1)*K1 Zero;F(2)*K1 Zero]);
        lamb1  = eig(Ad1);
        lamb1  = sort(abs(lamb1));
        lambda = lamb1(end-2);
        
    end

end