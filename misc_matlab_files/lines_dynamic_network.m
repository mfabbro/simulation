%Dynamic Grid NEtwork creator.
MAIN_TOP = triu(csvread('Network2.csv'));
no_tops = 700;
N = 4;
PROB = 0.2;
WINDOW_LENGTH = 3;
current_win_graph = zeros(N^2);
count = 0;
filename_gif = 'lines.gif';
for i = 1 : no_tops 
    adj = MAIN_TOP;
    new_window = 0;
    if(mod(i,WINDOW_LENGTH)==0)
        new_window = 1;
        if (current_win_graph > 0) == adj

        else
        adj = 2*(adj - (current_win_graph > 0)) + adj;
        count = count + 1;
        end
    end 


    for j = 1 : size(adj,1)-1
        temp_neighs = adj(j,:);
        ind = find(temp_neighs == 1);
  %      ind(randi(length(ind),1)) = [];
        temp_neighs(ind) = (rand(size(ind))>PROB);
        adj(j,:) = temp_neighs;
    end
    
    adj_matrix = (adj>0);
    filename = sprintf('line_tops/dynamic_lines_%d.csv',i);
    csvwrite(filename,adj_matrix+adj_matrix')
    if(new_window)
        current_win_graph = zeros(N^2);
    else
        current_win_graph = current_win_graph + adj_matrix;
    end
%    %%          
%         if(i<30)
%         p = nan(3*nnz(adj_matrix),2);
%         [x,j] = find(adj_matrix);
% 
%         p(1:3:end,:) = pts_array(x,:);
%         p(2:3:end,:) = pts_array(j,:);
%         set(edges, 'XData',p(:,1), 'YData',p(:,2))
% 
%         set(pts, 'XData',pts_array(:,1), 'YData',pts_array(:,2))
%         set(root, 'XData',pts_array(1,1), 'YData',pts_array(1,2))
%         if(i==1)
%         txt = text(pts_array(:,1)+0.01, pts_array(:,2)+0.01, ...
%                 num2str((1:size(pts_array,1))'), ...
%                 'HitTest','off', 'FontSize',12, ...
%                 'VerticalAlign','bottom', 'HorizontalAlign','left');
%         end
%             axis square
%             drawnow;
%             frame = getframe(1);
%             im = frame2im(frame);
%             [imind,cm] = rgb2ind(im,256);
%             if (i == 1)
%             imwrite(imind,cm,filename_gif,'gif', 'Loopcount',inf);
%             else
%             imwrite(imind,cm,filename_gif,'gif','WriteMode','append');
%             end
%         end
        
end
